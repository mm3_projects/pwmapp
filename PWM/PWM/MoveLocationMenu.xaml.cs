﻿using PWM.Core;
using PWM.Logic;
using PWM.Models.Log;
using System;
using System.Linq;
using System.Reflection;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PWM {
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MoveLocationMenu : MasterDetailPage {
        #region dependencies
        LogLogic logLogic = new LogLogic();
        #endregion

        #region properties
        public MoveLocation moveLocation { get; set; }
        #endregion

        public MoveLocationMenu () {
            InitializeComponent();

            this.moveLocation = new MoveLocation();
            Detail = new NavigationPage(this.moveLocation);
            IsPresented = false;
        }

        private void GoLimparDados (object sender, System.EventArgs e) {
            IsPresented = false;
            Application.Current.MainPage = new MoveLocationMenu();
        }

        private async void GoToMenu (object sender, System.EventArgs e) {
            var mensagem = "Tem a certeza que quer voltar ao menu?";

            if (App.systemCache.MoveLocation.pocos.Count() > 0) {
                mensagem = "Falta enviar dados para o M3. Irá perder informação. Tem a certeza que quer voltar ao menu?";
            }

            bool answer = await DisplayAlert("Localização", mensagem, "Sim", "Não");
            if (answer) {
                try {
                    App.systemCache.MoveLocationClearData();

                    IsPresented = false;
                    Application.Current.MainPage = new MenuMenu();
                } catch (Exception ex) {
                    logLogic.Insert(ex.Message, LogType.ERRO, MethodBase.GetCurrentMethod().DeclaringType.Name, App.systemCache.UserData.UserName);
                    await DisplayAlert("Localização", "Ocorreu um erro ao terminar movimentação de localização", "Sim", "Não");
                    return;
                }
            }
        }

        private void GoRunning (object sender, System.EventArgs e) {
            IsPresented = false;
            Application.Current.MainPage = new MoveLocationRunning();
        }

        private void GoPesquisarLocal (object sender, System.EventArgs e) {
            IsPresented = false;
            Application.Current.MainPage = new SearchByLocal(GlobalFunctions.GetMenuNamespace(this.GetType().Namespace, this.GetType().Name));
        }

        private void GoPesquisarProduto (object sender, System.EventArgs e) {
            IsPresented = false;
            Application.Current.MainPage = new SearchByProduct(GlobalFunctions.GetMenuNamespace(this.GetType().Namespace, this.GetType().Name));
        }
    }
}