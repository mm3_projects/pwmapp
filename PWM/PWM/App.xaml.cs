﻿using PWM.Core;
using PWM.Logic;
using PWM.Models.Log;
using System;
using System.Reflection;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PWM {
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class App : Application {
        #region properties
        public static SystemCache systemCache { get; set; }
        #endregion

        #region dependencies
        LogLogic logLogic = new LogLogic();
        #endregion

        public App () {
            try {
                InitializeComponent();

                MainPage = new Login();
            } catch (Exception ex) {
                logLogic.Insert("Erro App:" + ex.Message, LogType.ERRO, MethodBase.GetCurrentMethod().DeclaringType.Name, App.systemCache.UserData.UserName);
            }
        }

        protected override void OnStart () {
            // Handle when your app starts
        }

        protected override void OnSleep () {
            // Handle when your app sleeps
        }

        protected override void OnResume () {
            // Handle when your app resumes
        }
    }
}