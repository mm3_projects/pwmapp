﻿namespace PWM.Core {
    public interface IAppVersion {
        string GetVersionNumber ();
    }
}
