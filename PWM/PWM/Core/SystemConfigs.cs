﻿namespace PWM.Core {
    public static class SystemConfigs {
        public static string DateFormat {
            get {
                return @"yyyy-MM-dd HH:mm:ss.fff";
            }
        }

        public static string M3Environment (bool isRealEnvironment) {
            if (isRealEnvironment) {
                return "M3FDBPRD";
            }

            return "M3FDBTST";
        }

        public static string EmailErros {
            get {
                return @"mm3@polisport.com";
            }
        }
    }
}
