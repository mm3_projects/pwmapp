﻿using PWM.Models.Mittra;
using System.Collections.Generic;

namespace PWM.Core {
    public static class Repository {
        public static string GetAllUsers () {
            return @"SELECT * FROM [dbo].[PWM_User] WHERE [IsEnable]='1'";
        }

        public static string GetByUserAndPassword (string user, string password) {
            return string.Format(@"SELECT * 
                                FROM [dbo].[PWM_User] 
                                WHERE [UserName]='{0}' and [Password]='{1}' AND [IsEnable]='1'",
                                user,
                                password);
        }

        public static string GetDeviceConfigs (string serial) {
            return string.Format(@"SELECT rtrim(ltrim(DeviceSerial)) as DeviceSerial, 
                                    DeviceID,
                                    rtrim(ltrim(ConfigValue)) as ConfigValue, 
                                    ConfigID, 
                                    rtrim(ltrim(ConfigName)) as ConfigName
                                    FROM [dbo].[DeviceConfigView] 
                                    WHERE [DeviceSerial]='{0}'",
                                    serial);

        }

        public static string GetUserPermissions (string userID) {
            return string.Format(@"SELECT * 
                                FROM [dbo].[PWM_Menu_Permissions] 
                                WHERE [UserID]='{0}'",
                                userID);
        }

        public static string UpdateWarehouse (string configValue, long configID, long deviceID) {
            return string.Format(@"UPDATE [dbo].[PWM_Device_Config] 
                                SET [ConfigValue]='{0}' 
                                WHERE [ConfigID]='{1}' AND [DeviceID]='{2}'",
                                configValue,
                                configID,
                                deviceID);
        }

        public static string GetExpeditionBlocked (List<string> expeditionNumbers) {
            return string.Format(@"SELECT * 
                                   FROM [dbo].[PWM_Expedition_Blocked] 
                                   WHERE [ExpeditionNumber] in ('{0}')",
                                   string.Join("','", expeditionNumbers));
        }

        public static string InsertExpeditionBlocked (long userID, string expeditionNumber, long deviceID) {
            return string.Format(@"INSERT INTO [dbo].[PWM_Expedition_Blocked] ([UserID],[ExpeditionNumber], [DeviceID])
                                        VALUES('{0}','{1}','{2}')",
                                        userID,
                                        expeditionNumber,
                                        deviceID);
        }

        public static string RemoveExpeditionBlocked (List<string> expeditionNumbers, long userID, long deviceID) {
            return string.Format(@"DELETE FROM [dbo].[PWM_Expedition_Blocked]
                                   WHERE [ExpeditionNumber] in ('{0}') 
                                   AND [UserID]='{1}'
                                   AND [DeviceID]='{2}'",
                                   string.Join("','", expeditionNumbers),
                                   userID,
                                   deviceID);

        }

        public static string InsertLog (string log, string logType, string menu, string utilizador) {
            return string.Format(@"EXEC [dbo].[PWMInsertLog] '{0}','{1}','{2}','{3}'", log, logType, menu, utilizador);
        }

        public static string InsertMittra (MittraPoco poco) {
            var query = string.Format(@"INSERT INTO [DW].[dbo].[MITTRA]([MTCONO],[MTWHLO],[MTITNO],[CREATEDATE],[TRANSACTIONDATE],[MTRESP],[MTTTID]
                                ,[MTTTYP],[MTSLTP],[MTWHSL],[MTBANO],[MTRIDN],[MTRIDO],[MTRIDL],[MTRIDI],[MTPLSX],[MTTRQT])
                                VALUES('{0}','{1}','{2}', '{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}','{11}','{12}','{13}','{14}','{15}','{16}')",
                                poco.MTCONO,
                                poco.MTWHLO,
                                poco.MTITNO,
                                poco.CREATEDATE.ToString(SystemConfigs.DateFormat),
                                poco.TRANSACTIONDATE.ToString(SystemConfigs.DateFormat),
                                poco.MTRESP,
                                poco.MTTTID,
                                poco.MTTTYP,
                                poco.MTSLTP,
                                poco.MTWHSL,
                                poco.MTBANO,
                                poco.MTRIDN,
                                poco.MTRIDO.HasValue ? poco.MTRIDO.Value.ToString() : "NULL",
                                poco.MTRIDL,
                                poco.MTRIDI,
                                poco.MTPLSX,
                                poco.MTTRQT);

            query = query.Replace("'NULL'", "NULL");
            return query;
        }

        public static string GetWarehouses (string database, string facility) {
            return string.Format(@"EXEC [dbo].[PWMGetWarehouses] '{0}', '{1}'", database, facility);
        }

        public static string GetItemMitpop (string database, string item) {
            return string.Format(@"EXEC [dbo].[PWMGetItemMitpop] '{0}','{1}'",
                                database,
                                item);
        }

        public static string GetItemMitmas (string database, string item) {
            return string.Format(@"EXEC [dbo].[PWMGetItemMitmas] '{0}','{1}'",
                                database,
                                item);
        }

        public static string SearchByLocal (string database, string company, string facility, string warehouse) {
            return string.Format(@"EXEC [dbo].[PWMSearchByLocal] '{0}','{1}','{2}','{3}'",
                                database,
                                company,
                                facility,
                                warehouse);
        }

        public static string GetStock (string database, string company, string facility, string item, string warehouse, string location) {
            return string.Format(@"EXEC [dbo].[PWMGetStock] '{0}','{1}','{2}','{3}','{4}','{5}'",
                                database,
                                company,
                                facility,
                                item,
                                warehouse,
                                location);
        }

        public static string GetBoxType (string database, string boxType) {
            return string.Format(@"EXEC [dbo].[PWMGetBoxType] '{0}','{1}'",
                                database,
                                boxType);
        }

        public static string GetBoxQuantity (string database, string item) {
            return string.Format(@"EXEC [dbo].[PWMGetBoxQuantity] '{0}','{1}'",
                                database,
                                item);
        }

        public static string SearchByProduct (string database, string company, string facility, string item, string warehouse) {
            return string.Format(@"EXEC [dbo].[PWMSearchByProduct] '{0}','{1}','{2}','{3}','{4}'",
                                database,
                                company,
                                facility,
                                item,
                                warehouse);
        }

        public static string GetPackedExpeditions (string database, string company, string warehouse, List<long> deliveries) {
            return string.Format(@"EXEC [dbo].[PWMGetPackedExpeditions] '{0}','{1}','{2}','{3}'",
                                database,
                                company,
                                warehouse,
                                 "''" + string.Join("'',''", deliveries) + "''");
        }

        public static string GetExpedition (string database, string company, string division, string facility, string warehouse, long expedition, long delivery, int sufix, List<string> ignoreClientAlias) {
            return string.Format(@"EXEC [dbo].[PWMGetExpedition] '{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}'",
                                database,
                                company,
                                division,
                                facility,
                                warehouse,
                                expedition,
                                delivery,
                                sufix,
                                "''" + string.Join("'',''", ignoreClientAlias) + "''");
        }
    }
}
