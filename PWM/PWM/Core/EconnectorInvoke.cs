﻿namespace PWM.Core {
    using Newtonsoft.Json;
    using System.Collections.Generic;
    using System.IO;
    using System.Net;

    namespace PolisportCore {
        public class EconnetorInvoke {
            public class EnderecosPedidos {
                public string Nome { get; set; }
                public string Endpoint { get; set; }
                public string Pass { get; set; }
                public string Metodo { get; set; }

                protected static string EndpointAddressEconnetor {
                    get {

#if !DEBUG
                     return "https://descartes.polisport.com:4446/polisportapi/";
#endif

#if DEBUG
                        return "https://descartes.polisport.com:4446/polisportapitst/";
#endif
                    }
                }

                public static EnderecosPedidos GeneralGetData { get { return new EnderecosPedidos() { Nome = "getdata", Metodo = "POST", Endpoint = EndpointAddressEconnetor, Pass = "c79e4f47-e10a-4ac9-a6e5-49de66928dbf" }; } }
                public static EnderecosPedidos GeneralExecuteData { get { return new EnderecosPedidos() { Nome = "executedata", Metodo = "POST", Endpoint = EndpointAddressEconnetor, Pass = "c79e4f47-e10a-4ac9-a6e5-49de66928dbf" }; } }
                public static EnderecosPedidos GeneralGetConfigs { get { return new EnderecosPedidos() { Nome = "getconfigs", Metodo = "GET", Endpoint = EndpointAddressEconnetor, Pass = "c79e4f47-e10a-4ac9-a6e5-49de66928dbf" }; } }
                public static EnderecosPedidos SendEmail { get { return new EnderecosPedidos() { Nome = "sendemail", Metodo = "POST", Endpoint = EndpointAddressEconnetor, Pass = "c79e4f47-e10a-4ac9-a6e5-49de66928dbf" }; } }
                public static EnderecosPedidos Pack { get { return new EnderecosPedidos() { Nome = "packing", Metodo = "POST", Endpoint = EndpointAddressEconnetor, Pass = "c79e4f47-e10a-4ac9-a6e5-49de66928dbf" }; } }
                public static EnderecosPedidos MoveLocation { get { return new EnderecosPedidos() { Nome = "movelocation", Metodo = "POST", Endpoint = EndpointAddressEconnetor, Pass = "c79e4f47-e10a-4ac9-a6e5-49de66928dbf" }; } }
            }

            public static WebResponse EndpoinConnector<T> (EnderecosPedidos EnderecosPedidos, T Anexo) {
                System.Net.ServicePointManager.ServerCertificateValidationCallback =
                (object se, System.Security.Cryptography.X509Certificates.X509Certificate cert, System.Security.Cryptography.X509Certificates.X509Chain chain,
                System.Net.Security.SslPolicyErrors sslerror) => true;

                string encoded = System.Convert.ToBase64String(System.Text.Encoding.GetEncoding("ISO-8859-1").GetBytes(EnderecosPedidos.Pass));
                var endpointLink = EnderecosPedidos.Endpoint + EnderecosPedidos.Nome;
                HttpWebRequest request = null;

                request = WebRequest.Create(endpointLink) as HttpWebRequest;
                request.Headers.Add("Authorization", "Basic " + encoded);
                request.AuthenticationLevel = System.Net.Security.AuthenticationLevel.MutualAuthRequested;
                request.Method = EnderecosPedidos.Metodo;
                request.Timeout = 60000;

                if (Anexo != null) {
                    byte[] bytes;
                    string jsonstr = JsonConvert.SerializeObject(Anexo);
                    bytes = System.Text.Encoding.UTF8.GetBytes(jsonstr);
                    request.ContentLength = bytes.Length;
                    request.ContentType = "application/json";
                    Stream requestStream = request.GetRequestStream();
                    requestStream.Write(bytes, 0, bytes.Length);
                    requestStream.Close();
                    HttpWebResponse response = (HttpWebResponse)request.GetResponse();

                }
                return request.GetResponse();
            }

            public static WebResponse EndpoinConnector<T> (EnderecosPedidos EnderecosPedidos, T Anexo, string parametro) {
                System.Net.ServicePointManager.ServerCertificateValidationCallback =
                (object se, System.Security.Cryptography.X509Certificates.X509Certificate cert, System.Security.Cryptography.X509Certificates.X509Chain chain,
                System.Net.Security.SslPolicyErrors sslerror) => true;

                string encoded = System.Convert.ToBase64String(System.Text.Encoding.GetEncoding("ISO-8859-1").GetBytes(EnderecosPedidos.Pass));
                var url = string.Format(EnderecosPedidos.Nome, parametro);
                var endpointLink = EnderecosPedidos.Endpoint + url;
                HttpWebRequest request = null;

                request = WebRequest.Create(endpointLink) as HttpWebRequest;
                request.Headers.Add("Authorization", "Basic " + encoded);
                request.AuthenticationLevel = System.Net.Security.AuthenticationLevel.MutualAuthRequested;
                request.Method = EnderecosPedidos.Metodo;
                request.Timeout = 60000;

                if (Anexo != null) {
                    byte[] bytes;
                    string jsonstr = JsonConvert.SerializeObject(Anexo);
                    bytes = System.Text.Encoding.UTF8.GetBytes(jsonstr);
                    request.ContentLength = bytes.Length;
                    request.ContentType = "text/json";
                    Stream requestStream = request.GetRequestStream();
                    requestStream.Write(bytes, 0, bytes.Length);
                    requestStream.Close();
                    HttpWebResponse response = (HttpWebResponse)request.GetResponse();

                }
                return request.GetResponse();
            }

            public static WebResponse EndpoinConnector (EnderecosPedidos EnderecosPedidos, string parametro) {
                System.Net.ServicePointManager.ServerCertificateValidationCallback =
                (object se, System.Security.Cryptography.X509Certificates.X509Certificate cert, System.Security.Cryptography.X509Certificates.X509Chain chain,
                System.Net.Security.SslPolicyErrors sslerror) => true;

                string encoded = System.Convert.ToBase64String(System.Text.Encoding.GetEncoding("ISO-8859-1").GetBytes(EnderecosPedidos.Pass));
                string endpointLink = null;

                HttpWebRequest request = null;

                endpointLink = EnderecosPedidos.Endpoint + EnderecosPedidos.Nome + "/" + parametro;

                request = WebRequest.Create(endpointLink) as HttpWebRequest;
                request.Headers.Add("Authorization", "Basic " + encoded);
                request.AuthenticationLevel = System.Net.Security.AuthenticationLevel.MutualAuthRequested;
                request.Method = EnderecosPedidos.Metodo;

                return request.GetResponse();
            }

            public static WebResponse EndpoinConnector (EnderecosPedidos EnderecosPedidos) {
                System.Net.ServicePointManager.ServerCertificateValidationCallback =
                (object se, System.Security.Cryptography.X509Certificates.X509Certificate cert, System.Security.Cryptography.X509Certificates.X509Chain chain,
                System.Net.Security.SslPolicyErrors sslerror) => true;

                string encoded = System.Convert.ToBase64String(System.Text.Encoding.GetEncoding("ISO-8859-1").GetBytes(EnderecosPedidos.Pass));
                string endpointLink = null;

                HttpWebRequest request = null;

                endpointLink = EnderecosPedidos.Endpoint + EnderecosPedidos.Nome;

                request = WebRequest.Create(endpointLink) as HttpWebRequest;
                request.Headers.Add("Authorization", "Basic " + encoded);
                request.AuthenticationLevel = System.Net.Security.AuthenticationLevel.MutualAuthRequested;
                request.Method = EnderecosPedidos.Metodo;

                return request.GetResponse();
            }

            public static WebResponse EndpoinConnector<T> (EnderecosPedidos EnderecosPedidos, List<T> Anexo) {
                System.Net.ServicePointManager.ServerCertificateValidationCallback =
                (object se, System.Security.Cryptography.X509Certificates.X509Certificate cert, System.Security.Cryptography.X509Certificates.X509Chain chain,
                System.Net.Security.SslPolicyErrors sslerror) => true;

                string encoded = System.Convert.ToBase64String(System.Text.Encoding.GetEncoding("ISO-8859-1").GetBytes(EnderecosPedidos.Pass));
                string endpointLink = null;

                HttpWebRequest request = null;

                endpointLink = EnderecosPedidos.Endpoint + EnderecosPedidos.Nome;

                request = WebRequest.Create(endpointLink) as HttpWebRequest;
                request.Headers.Add("Authorization", "Basic " + encoded);
                request.AuthenticationLevel = System.Net.Security.AuthenticationLevel.MutualAuthRequested;
                request.Method = EnderecosPedidos.Metodo;

                if (Anexo != null) {
                    byte[] bytes;
                    string jsonstr = JsonConvert.SerializeObject(Anexo);
                    bytes = System.Text.Encoding.UTF8.GetBytes(jsonstr);
                    request.ContentLength = bytes.Length;
                    request.ContentType = "text/json";
                    Stream requestStream = request.GetRequestStream();
                    requestStream.Write(bytes, 0, bytes.Length);
                    requestStream.Close();
                    HttpWebResponse response = (HttpWebResponse)request.GetResponse();

                }
                return request.GetResponse();
            }

            public static WebResponse EndpoinConnector<T1, T2> (EnderecosPedidos EnderecosPedidos, T1 Anexo, List<T2> parametros = null) {
                HttpWebRequest request = null;

                try {
                    System.Net.ServicePointManager.ServerCertificateValidationCallback =
                    (object se, System.Security.Cryptography.X509Certificates.X509Certificate cert, System.Security.Cryptography.X509Certificates.X509Chain chain,
                    System.Net.Security.SslPolicyErrors sslerror) => true;

                    string encoded = System.Convert.ToBase64String(System.Text.Encoding.GetEncoding("ISO-8859-1").GetBytes(EnderecosPedidos.Pass));
                    string endpointLink = null;

                    endpointLink = EnderecosPedidos.Endpoint + EnderecosPedidos.Nome;

                    if (parametros != null)
                        parametros.ForEach(p => endpointLink += "/" + p);

                    request = WebRequest.Create(endpointLink) as HttpWebRequest;
                    request.Headers.Add("Authorization", "Basic " + encoded);
                    request.AuthenticationLevel = System.Net.Security.AuthenticationLevel.MutualAuthRequested;
                    request.Method = EnderecosPedidos.Metodo;

                    if (Anexo != null) {
                        byte[] bytes;
                        string jsonstr = JsonConvert.SerializeObject(Anexo);
                        bytes = System.Text.Encoding.UTF8.GetBytes(jsonstr);
                        request.ContentLength = bytes.Length;
                        request.ContentType = "text/json";
                        Stream requestStream = request.GetRequestStream();
                        requestStream.Write(bytes, 0, bytes.Length);
                        requestStream.Close();
                        HttpWebResponse response = (HttpWebResponse)request.GetResponse();

                    }
                    return request.GetResponse();
                } catch {
                    HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                    return response;
                }

            }

            public static WebResponse EndpoinConnector<T2> (EnderecosPedidos EnderecosPedidos, MemoryStream anexo, List<T2> parametros = null) {
                HttpWebRequest request = null;

                try {
                    System.Net.ServicePointManager.ServerCertificateValidationCallback =
                    (object se, System.Security.Cryptography.X509Certificates.X509Certificate cert, System.Security.Cryptography.X509Certificates.X509Chain chain,
                    System.Net.Security.SslPolicyErrors sslerror) => true;

                    string encoded = System.Convert.ToBase64String(System.Text.Encoding.GetEncoding("ISO-8859-1").GetBytes(EnderecosPedidos.Pass));
                    string endpointLink = null;

                    endpointLink = EnderecosPedidos.Endpoint + EnderecosPedidos.Nome;

                    if (parametros != null)
                        parametros.ForEach(p => endpointLink += "/" + p);

                    request = WebRequest.Create(endpointLink) as HttpWebRequest;
                    request.Headers.Add("Authorization", "Basic " + encoded);
                    request.AuthenticationLevel = System.Net.Security.AuthenticationLevel.MutualAuthRequested;
                    request.Method = EnderecosPedidos.Metodo;

                    anexo.CopyTo(request.GetRequestStream());
                    HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                    return request.GetResponse();
                } catch {
                    HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                    return response;
                }

            }
        }
    }

}
