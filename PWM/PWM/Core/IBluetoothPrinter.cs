﻿namespace PWM.Core {
    public interface IBluetoothPrinter {
        string Print (string printerName, string alias);
    }
}
