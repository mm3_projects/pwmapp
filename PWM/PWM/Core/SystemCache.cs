﻿using PWM.Models;
using PWM.Models.Device;
using PWM.Models.Expedition;
using PWM.Models.MoveLocation;
using PWM.Models.PWMConfigs;
using PWM.Models.User;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace PWM.Core {
    public class SystemCache {
        public UserModelCache UserData { get; set; }

        public GlobalConfigsCache GlobalConfigsCache { get; set; }

        public DeviceModelCache DeviceData { get; set; }

        public ExpeditionModelCache ExpeditionData { get; set; }

        public MoveLocationCache MoveLocation { get; set; }

        public SystemCache () {
            UserData = new UserModelCache();
            DeviceData = new DeviceModelCache();
            ExpeditionData = new ExpeditionModelCache();
            MoveLocation = new MoveLocationCache();
            GlobalConfigsCache = new GlobalConfigsCache();
        }

        public void ExpeditionClearAllData () {
            ExpeditionData.ExpeditionDeliveryLbl = string.Empty;
            ExpeditionData.ExpeditionDelivery = 0;
            ExpeditionData.ExpeditionBox = string.Empty;
            ExpeditionData.ExpeditionLocal = string.Empty;
            ExpeditionData.ExpeditionLocalLbl = string.Empty;
            ExpeditionData.ExpeditionLot = string.Empty;
            ExpeditionData.ExpeditionProduct = string.Empty;
            ExpeditionData.ExpeditionOrdersPicked = new ObservableCollection<ExpeditionReadModel>();
            ExpeditionData.ExpeditionInfo = new ObservableCollection<ExpeditionModel>();
            ExpeditionData.ExpeditionInfoFiltered = new ObservableCollection<ExpeditionModel>();
            ExpeditionData.DeliveryBoxDic = new System.Collections.Generic.Dictionary<long, System.Collections.Generic.List<string>>();
            ExpeditionData.DeliveryTipoCaixa = new System.Collections.Generic.Dictionary<long, string>();
            ExpeditionData.FailedPacking = new System.Collections.Generic.List<PackingModel>();
        }

        public void ExpeditionClearData () {
            ExpeditionData.ExpeditionDeliveryLbl = string.Empty;
            ExpeditionData.ExpeditionBox = string.Empty;
            ExpeditionData.ExpeditionDelivery = 0;
            ExpeditionData.ExpeditionLot = string.Empty;
            ExpeditionData.ExpeditionProduct = string.Empty;
            ExpeditionData.ExpeditionLocal = string.Empty;
            ExpeditionData.ExpeditionLocalLbl = string.Empty;
        }

        public void MoveLocationClearData () {
            MoveLocation.ItemDescriptionlbl = string.Empty;
            MoveLocation.Itemlbl = string.Empty;
            MoveLocation.Local = string.Empty;
            MoveLocation.Lot = string.Empty;
            MoveLocation.Lotlbl = string.Empty;
            MoveLocation.pocos = new List<MoveLocationPoco>();
            MoveLocation.Warehouse = string.Empty;
            MoveLocation.StockQuantity = 0;
            MoveLocation.ItemStandardQuantity = 0;
        }
    }
}
