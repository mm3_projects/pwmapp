﻿using PWM.Core;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PWM
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Help : ContentPage {
        public Help () {
			InitializeComponent ();

            this.lblAmbienteText.Text = App.systemCache.DeviceData.RealEnvironment == true ? "Real" : "Teste";
            this.lblSerialText.Text = App.systemCache.DeviceData.DeviceSerial;
            this.lblVersionText.Text = "v" + DependencyService.Get<IAppVersion>().GetVersionNumber();
        }
	}
}