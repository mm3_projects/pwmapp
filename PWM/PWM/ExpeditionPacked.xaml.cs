﻿using Acr.UserDialogs;
using PWM.Logic;
using PWM.Models;
using PWM.Models.Log;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PWM {
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ExpeditionPacked : ContentPage {
        #region dependencies
        M3Logic m3Logic = new M3Logic();
        LogLogic logLogic = new LogLogic();
        #endregion

        #region properties
        public int rowSize = 40;
        public int columnSize = 100;
        #endregion

        public ExpeditionPacked () {
            InitializeComponent();
            FillDataAndAdjustView();
        }

        private void FillDataAndAdjustView () {
            try {
                var getM3data = m3Logic.GetPackedExpeditions(App.systemCache.DeviceData.RealEnvironment, App.systemCache.DeviceData.Company, App.systemCache.DeviceData.Warehouse, App.systemCache.ExpeditionData.ExpeditionInfo.ToList().Select(x => x.DeliveryNumber).Distinct().ToList());

                var data = new ObservableCollection<ExpeditionPackedModel>();
                getM3data.ForEach(x => data.Add(x));

                this.ListViewExpeditionPacked.ItemsSource = data;

                var rowsCount = App.systemCache.ExpeditionData.ExpeditionInfo.Count;
                var columnsCount = 4;

                this.ListViewExpeditionPacked.HeightRequest = rowSize;
                this.ListViewExpeditionPacked.WidthRequest = (columnsCount * columnSize);
            } catch (Exception ex) {
                DisplayAlert("Expedição", ex.Message, "OK");
                logLogic.Insert("Erro FillDataAndAdjustView:" + ex.Message, LogType.ERRO, MethodBase.GetCurrentMethod().DeclaringType.Name, App.systemCache.UserData.UserName);
            }
        }

        private async void ReimprimirButton_Clicked (object sender, EventArgs e) {
            using (UserDialogs.Instance.Loading("Loading...", null, null, true)) {
                var response = await Task.Run(() => Reprint());

                if (!string.IsNullOrEmpty(response)) {
                    this.ListViewExpeditionPacked.SelectedItem = null;
                    await DisplayAlert("ExpediçãoEmbalados", response, "OK");
                    return;
                }
            }
        }

        private string Reprint () {
            try {
                if (this.ListViewExpeditionPacked.SelectedItem == null) {
                    return "Tem que selecionar a linha que pretende reimprimir";
                }

                var item = (ExpeditionPackedModel)this.ListViewExpeditionPacked.SelectedItem;
                var aliasPoco = App.systemCache.ExpeditionData.ExpeditionInfo.Where(x => x.Item == item.Item.Trim()).FirstOrDefault();

                if (aliasPoco == null || string.IsNullOrEmpty(aliasPoco.Alias)) {
                    return "Não existe alias para este artigo";
                }

                var printLogic = new PrintLogic();
                var resultPrint = printLogic.Print(App.systemCache.DeviceData.FileNameToPrint, App.systemCache.DeviceData.PrinterName, aliasPoco.Alias, item.Item, App.systemCache.UserData.UserID);

                if (!string.IsNullOrEmpty(resultPrint)) {
                    return "Erro ao reimprimir:" + resultPrint;
                }

                return string.Empty;
            } catch (Exception ex) {
                logLogic.Insert(ex.Message, LogType.ERRO, MethodBase.GetCurrentMethod().DeclaringType.Name, App.systemCache.UserData.UserName);
                return "Erro:" + ex.Message;
            }
        }

        private void TapGestureBackButton_Tapped (object sender, EventArgs e) {
            Application.Current.MainPage = new ExpeditionMenu();
        }
    }
}