﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PWM {
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Configurations : ContentPage {
        public Configurations () {
            InitializeComponent();
        }

        private void ButtonSave_Clicked (object sender, EventArgs e) {

        }

        private void ButtonCancel_Clicked (object sender, EventArgs e) {
            Application.Current.MainPage = new MenuMenu();
        }
    }
}