﻿using PWM.Logic;
using PWM.Models.Log;
using PWM.Models.Menu;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PWM {
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Menu : ContentPage {
        #region properties
        public int numberOfColumns = 3;
        #endregion

        #region dependencies
        LogLogic logLogic = new LogLogic();
        #endregion

        public Menu () {
            try {
                InitializeComponent();
                this.labelHeader.Text = "Armazém - " + App.systemCache.DeviceData.Warehouse;

                MenuTable.Children.Add(CreateMenuTable());
                base.OnAppearing();
            } catch (Exception ex) {
                DisplayAlert("Menu", ex.Message, "OK");
                logLogic.Insert("Erro Menu:" + ex.Message, LogType.ERRO, MethodBase.GetCurrentMethod().DeclaringType.Name, App.systemCache.UserData.UserName);
            }
        }

        public ScrollView CreateMenuTable () {
            var scrollView = new ScrollView() {
                VerticalScrollBarVisibility = ScrollBarVisibility.Always,
                HorizontalScrollBarVisibility = ScrollBarVisibility.Never,
                Orientation = ScrollOrientation.Vertical
            };

            var stackLayout = new StackLayout() {
                HorizontalOptions = LayoutOptions.CenterAndExpand,
                VerticalOptions = LayoutOptions.FillAndExpand
            };

            var menuToCreate = GetMenuToCreate(App.systemCache.UserData.Permissions);
            var rowIndex = 0;
            var dataTemplate = new DataTemplate(() => {
                var dashboard = new Grid() {
                    ColumnSpacing = 0,
                    RowSpacing = 0
                };

                dashboard.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
                dashboard.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
                dashboard.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
                dashboard.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
                dashboard.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });

                var model = menuToCreate.ElementAt(rowIndex);

                #region create title
                var title1 = new Label {
                    FontAttributes = FontAttributes.Bold,
                    TextColor = Color.Black,
                    VerticalOptions = LayoutOptions.Start,
                    HorizontalOptions = LayoutOptions.Center,
                    Text = model.Title1
                };

                var title2 = new Label {
                    FontAttributes = FontAttributes.Bold,
                    TextColor = Color.Black,
                    VerticalOptions = LayoutOptions.Start,
                    HorizontalOptions = LayoutOptions.Center,
                    Text = model.Title2
                };

                var title3 = new Label {
                    FontAttributes = FontAttributes.Bold,
                    TextColor = Color.Black,
                    VerticalOptions = LayoutOptions.Start,
                    HorizontalOptions = LayoutOptions.Center,
                    Text = model.Title3
                };
                #endregion

                #region create image
                TapGestureRecognizer imageTap1 = new TapGestureRecognizer();
                if (!string.IsNullOrEmpty(model.NavigateType1)) {
                    imageTap1.Tapped += (s, e) => {
                        var page = Activator.CreateInstance(Type.GetType(model.NavigateType1)) as Page;
                        Application.Current.MainPage = page;
                    };
                }

                TapGestureRecognizer imageTap2 = new TapGestureRecognizer();
                if (!string.IsNullOrEmpty(model.NavigateType2)) {
                    imageTap2.Tapped += (s, e) => {
                        var page = Activator.CreateInstance(Type.GetType(model.NavigateType2)) as Page;
                        Application.Current.MainPage = page;
                    };
                }

                TapGestureRecognizer imageTap3 = new TapGestureRecognizer();
                if (!string.IsNullOrEmpty(model.NavigateType3)) {
                    imageTap3.Tapped += (s, e) => {
                        var page = Activator.CreateInstance(Type.GetType(model.NavigateType3)) as Page;
                        Application.Current.MainPage = page;
                    };
                }

                var image1 = new Image { HeightRequest = 60, Source = model.Image1 };
                image1.GestureRecognizers.Add(imageTap1);

                var image2 = new Image { HeightRequest = 60, Source = model.Image2 };
                image2.GestureRecognizers.Add(imageTap2);

                var image3 = new Image { HeightRequest = 60, Source = model.Image3 };
                image3.GestureRecognizers.Add(imageTap3);
                #endregion

                //title1.SetBinding(Label.TextProperty, "Title1");
                //image1.SetBinding(Image.SourceProperty, "Image1");
                //title2.SetBinding(Label.TextProperty, "Title2");
                //image2.SetBinding(Image.SourceProperty, "Image2");
                //title3.SetBinding(Label.TextProperty, "Title3");
                //image3.SetBinding(Image.SourceProperty, "Image3");

                dashboard.Children.Add(title1, 0, 1);
                dashboard.Children.Add(title2, 1, 1);
                dashboard.Children.Add(title3, 2, 1);

                dashboard.Children.Add(image1, 0, 0);
                dashboard.Children.Add(image2, 1, 0);
                dashboard.Children.Add(image3, 2, 0);

                rowIndex += 1;

                return new ViewCell {
                    View = dashboard
                };
            });


            var listView = new ListView() {
                VerticalOptions = LayoutOptions.StartAndExpand,
                HasUnevenRows = true,
                HorizontalOptions = LayoutOptions.StartAndExpand,
                SelectionMode = ListViewSelectionMode.None,
                SeparatorVisibility = SeparatorVisibility.None,
                ItemsSource = menuToCreate,
                ItemTemplate = dataTemplate
            };

            stackLayout.Children.Add(listView);
            scrollView.Content = stackLayout;

            return scrollView;
        }

        private List<MenuModel> GetMenuToCreate (DataTable permissionsData) {
            var permissions = permissionsData.Rows.OfType<DataRow>().ToList();
            var menuToCreate = new List<MenuModel>();
            var stopRun = false;
            var dataToCheck = permissions;
            var dataToTake = numberOfColumns;
            var dataToSkip = 0;

            while (!stopRun) {
                if (permissions.Count() > numberOfColumns) {
                    dataToCheck = permissions.Skip(dataToSkip).Take(dataToTake).ToList();
                }

                var model = new MenuModel() {
                    Image1 = dataToCheck.Count() >= 1 ? dataToCheck[0]["Image"].ToString() : string.Empty,
                    Image2 = dataToCheck.Count() >= 2 ? dataToCheck[1]["Image"].ToString() : string.Empty,
                    Image3 = dataToCheck.Count() >= 3 ? dataToCheck[2]["Image"].ToString() : string.Empty,
                    NavigateType1 = dataToCheck.Count() >= 1 ? dataToCheck[0]["Navigation"].ToString() : null,
                    NavigateType2 = dataToCheck.Count() >= 2 ? dataToCheck[1]["Navigation"].ToString() : null,
                    NavigateType3 = dataToCheck.Count() >= 3 ? dataToCheck[2]["Navigation"].ToString() : null,
                    Title1 = dataToCheck.Count() >= 1 ? dataToCheck[0]["Title"].ToString() : string.Empty,
                    Title2 = dataToCheck.Count() >= 2 ? dataToCheck[1]["Title"].ToString() : string.Empty,
                    Title3 = dataToCheck.Count() >= 3 ? dataToCheck[2]["Title"].ToString() : string.Empty
                };

                menuToCreate.Add(model);

                dataToSkip += dataToTake;

                if (dataToSkip >= permissions.Count) {
                    stopRun = true;
                }
            }

            return menuToCreate;
        }
    }
}