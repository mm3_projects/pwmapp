﻿using Acr.UserDialogs;
using PWM.Logic;
using PWM.Models.Log;
using System;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PWM {
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ExpeditionIncompleteBox : ContentPage {
        #region properties
        public long entrega { get; set; }
        public decimal missingQuantity { get; set; }
        #endregion

        #region dependencies
        M3Logic m3Logic = new M3Logic();
        ExpeditionLogic expeditionLogic = new ExpeditionLogic();
        LogLogic logLogic = new LogLogic();
        #endregion

        public ExpeditionIncompleteBox (long Entrega, decimal MissingQuantity) {
            try {
                InitializeComponent();
                this.entrega = Entrega;
                this.missingQuantity = MissingQuantity;
                this.TitleEntrega.Text = "Entrega:" + this.entrega;
                this.TitleQuantidade.Text = "Qtd. em falta:" + this.missingQuantity;

                if (App.systemCache.ExpeditionData.DeliveryTipoCaixa.ContainsKey(App.systemCache.ExpeditionData.ExpeditionDelivery)) {
                    this.TextBoxTipoCaixa.Text = App.systemCache.ExpeditionData.DeliveryTipoCaixa[App.systemCache.ExpeditionData.ExpeditionDelivery];
                }
            } catch (Exception ex) {
                DisplayAlert("Expedição", ex.Message, "OK");
                logLogic.Insert("Erro ExpeditionIncompleteBox:" + ex.Message, LogType.ERRO, MethodBase.GetCurrentMethod().DeclaringType.Name, App.systemCache.UserData.UserName);
            }
        }

        private void ButtonCancel_Clicked (object sender, EventArgs e) {
            Application.Current.MainPage = new ExpeditionMenu();
        }

        private async void ButtonConfirm_Clicked (object sender, EventArgs e) {
            using (UserDialogs.Instance.Loading("Loading...", null, null, true)) {
                var response = await Task.Run(() => Confirm());

                if (!string.IsNullOrEmpty(response)) {
                    await DisplayAlert("ExpediçãoCaixaIncompleta", response, "OK");
                    return;
                }

                Application.Current.MainPage = new ExpeditionMenu();
            }
        }

        private string Confirm () {
            try {
                if (this.TextBoxQuantidade.Text.Trim().Length == 0) {
                    return "Preencha o campo quantidade";
                }

                if (this.TextBoxTipoCaixa.Text.Trim().Length == 0) {
                    return "Preencha o campo tipo caixa";
                }

                int qty;
                if (!int.TryParse(this.TextBoxQuantidade.Text.Trim(), out qty)) {
                    return "Campo quantidade inválido";
                }

                if (qty <= 0) {
                    return "Quantidade deve ser superior a 0";
                }

                if (qty > this.missingQuantity) {
                    return "Quantidade digitada é superior à quantidade em falta";
                }

                var checkTipoCaixa = m3Logic.CheckIfExistTipoCaixa(App.systemCache.DeviceData.RealEnvironment, this.TextBoxTipoCaixa.Text.Trim().ToLower());

                if (checkTipoCaixa == 0) {
                    return "Tipo Caixa digitado não existe";
                }

                var resultLine = expeditionLogic.ProcessReading(this.TextBoxTipoCaixa.Text.Trim(), qty, true);

                if (!string.IsNullOrEmpty(resultLine.Item1)) {
                    return "Erro:" + resultLine.Item1;
                }

                var itemAlias = App.systemCache.ExpeditionData.ExpeditionInfoFiltered
                           .Where(x => x.DeliveryNumber == App.systemCache.ExpeditionData.ExpeditionDelivery && x.Item == App.systemCache.ExpeditionData.ExpeditionProduct).FirstOrDefault();

                if (itemAlias != null && !string.IsNullOrEmpty(itemAlias.Alias)) {
                    Task.Run(() => {
                        var printLogic = new PrintLogic();
                        printLogic.Print(App.systemCache.DeviceData.FileNameToPrint, App.systemCache.DeviceData.PrinterName, itemAlias.Alias, itemAlias.Item, App.systemCache.UserData.UserID);
                    });
                }

                if (App.systemCache.ExpeditionData.DeliveryTipoCaixa.ContainsKey(App.systemCache.ExpeditionData.ExpeditionDelivery)) {
                    App.systemCache.ExpeditionData.DeliveryTipoCaixa[App.systemCache.ExpeditionData.ExpeditionDelivery] = this.TextBoxTipoCaixa.Text.Trim().ToLower();
                } else {
                    App.systemCache.ExpeditionData.DeliveryTipoCaixa.Add(App.systemCache.ExpeditionData.ExpeditionDelivery, this.TextBoxTipoCaixa.Text.Trim().ToLower());
                }

                var dataToUpdate = App.systemCache.ExpeditionData.ExpeditionInfo.Where(x => x.MissingQty > 0 && x.LocationOrder == App.systemCache.ExpeditionData.ExpeditionLocal).ToList();

                if (dataToUpdate.Count() == 0) {
                    dataToUpdate = App.systemCache.ExpeditionData.ExpeditionInfo.Where(x => x.MissingQty > 0).ToList();
                }

                if (dataToUpdate.Count > 0 && dataToUpdate.Where(x => x.LocationOrder == App.systemCache.ExpeditionData.ExpeditionLocal).Count() == 0) {
                    App.systemCache.ExpeditionData.ExpeditionLocal = string.Empty;
                    App.systemCache.ExpeditionData.ExpeditionLocalLbl = string.Empty;
                }

                App.systemCache.ExpeditionData.ExpeditionInfoFiltered = new System.Collections.ObjectModel.ObservableCollection<Models.ExpeditionModel>();
                dataToUpdate.ForEach(x => App.systemCache.ExpeditionData.ExpeditionInfoFiltered.Add(x));

                return string.Empty;
            } catch (Exception ex) {
                logLogic.Insert(ex.Message, LogType.ERRO, MethodBase.GetCurrentMethod().DeclaringType.Name, App.systemCache.UserData.UserName);
                return "Erro:" + ex.Message;
            }
        }

        private void ContentPage_Appearing (object sender, EventArgs e) {
            this.TextBoxQuantidade.Focus();
        }
    }
}