﻿using Acr.UserDialogs;
using PWM.Logic;
using PWM.Models;
using PWM.Models.Expedition;
using PWM.Models.Log;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PWM {
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Expedition : ContentPage {
        #region dependencies
        M3Logic m3Logic = new M3Logic();
        ExpeditionLogic expeditionLogic = new ExpeditionLogic();
        LogLogic logLogic = new LogLogic();
        ReadsValidationLogic readsValidationLogic = new ReadsValidationLogic();
        #endregion

        #region properties
        public int rowSize = 40;
        public int columnSize = 80;
        public int TextBoxLength { get; set; }
        public int checkLot = 0;
        public bool HaveLot = false;
        #endregion

        public Expedition () {
            InitializeComponent();
            this.ButtonReenviarM3.IsVisible = false;
            this.lblCaixa.Text = string.Empty;
            this.lblLocal.Text = string.Empty;
            this.lblLote.Text = string.Empty;
            this.lblProduto.Text = string.Empty;
            this.TextBoxData.Placeholder = "Introduza o local";

            FillData();
        }

        private void FillData () {
            try {
                App.systemCache.ExpeditionData.ExpeditionInfoFiltered = App.systemCache.ExpeditionData.ExpeditionInfo;
                var dataFilter = App.systemCache.ExpeditionData.ExpeditionInfoFiltered
                    .Where(x => x.MissingQty > 0)
                    .ToList()
                    .OrderBy(x => x.LocationOrder)
                    .ToList();

                if (!string.IsNullOrEmpty(App.systemCache.ExpeditionData.ExpeditionLocal)) {
                    App.systemCache.ExpeditionData.ExpeditionDeliveryLbl = string.Empty;
                    App.systemCache.ExpeditionData.ExpeditionBox = string.Empty;
                    App.systemCache.ExpeditionData.ExpeditionDelivery = 0;
                    App.systemCache.ExpeditionData.ExpeditionLot = string.Empty;
                    App.systemCache.ExpeditionData.ExpeditionProduct = string.Empty;

                    this.lblLocal.Text = App.systemCache.ExpeditionData.ExpeditionLocalLbl;
                    this.TextBoxData.Placeholder = "Introduza o produto e o lote";

                    dataFilter = dataFilter.Where(x => x.LocationOrder.Trim().ToLower() == App.systemCache.ExpeditionData.ExpeditionLocal.Trim().ToLower()).ToList();

                    if (!dataFilter.Any()) {
                        dataFilter = App.systemCache.ExpeditionData.ExpeditionInfoFiltered
                            .Where(x => x.MissingQty > 0)
                            .ToList()
                            .OrderBy(x => x.LocationOrder)
                            .ToList();

                        this.lblLocal.Text = string.Empty;
                        this.lblLote.Text = string.Empty;
                        this.lblProduto.Text = string.Empty;
                        this.lblCaixa.Text = string.Empty;
                        this.TextBoxData.Placeholder = "Introduza o local";
                    }
                }

                var data = new ObservableCollection<ExpeditionModel>();
                dataFilter.ForEach(x => data.Add(x));

                App.systemCache.ExpeditionData.ExpeditionInfoFiltered = data;
                this.ListViewExpedition.ItemsSource = data;
                AdjustListViewSize();
            } catch (Exception ex) {
                DisplayAlert("Expedição", ex.Message, "OK");
                logLogic.Insert("Erro FillData:" + ex.Message, LogType.ERRO, MethodBase.GetCurrentMethod().DeclaringType.Name, App.systemCache.UserData.UserName);
            }
        }

        private void ButtonOk_Clicked (object sender, EventArgs e) {
            ValidarInput();
        }

        private void TextBoxData_Unfocused (object sender, FocusEventArgs e) {
            this.TextBoxData.Focus();
        }

        private async void ValidarInput () {
            using (UserDialogs.Instance.Loading("Loading...", null, null, true)) {
                try {
                    this.ButtonOk.IsEnabled = false;
                    var response = await Task.Run(() => ValidateInput());

                    #region local
                    if (!string.IsNullOrEmpty(response.ErroLocal)) {
                        this.lblLocal.Text = string.Empty;
                        this.ButtonOk.IsEnabled = true;
                        this.TextBoxData.Text = string.Empty;
                        await DisplayAlert("Expedição", response.ErroLocal, "OK");
                        return;
                    } else {
                        if (string.IsNullOrEmpty(this.lblLocal.Text)) {
                            this.lblLocal.Text = App.systemCache.ExpeditionData.ExpeditionLocalLbl;
                            this.TextBoxData.Placeholder = "Introduza o produto e o lote";

                            var data = new ObservableCollection<ExpeditionModel>();
                            response.ListByLocal.ForEach(x => data.Add(x));

                            App.systemCache.ExpeditionData.ExpeditionInfoFiltered = data;
                            this.ListViewExpedition.ItemsSource = data;
                            AdjustListViewSize();

                            this.ButtonOk.IsEnabled = true;
                            this.TextBoxData.Text = string.Empty;
                            return;
                        }
                    }
                    #endregion

                    #region produto/lote
                    if (!string.IsNullOrEmpty(response.ErroProdutoLote)) {
                        this.TextBoxData.Text = string.Empty;
                        this.lblProduto.Text = string.Empty;
                        this.ButtonOk.IsEnabled = true;
                        await DisplayAlert("Expedição", response.ErroProdutoLote, "OK");
                        return;
                    } else {
                        if (string.IsNullOrEmpty(lblProduto.Text) || (string.IsNullOrEmpty(lblLote.Text) && this.HaveLot)) {
                            var filterData = new List<ExpeditionModel>();
                            filterData = App.systemCache.ExpeditionData.ExpeditionInfoFiltered.Where(
                                      x => x.LocationOrder == App.systemCache.ExpeditionData.ExpeditionLocal &&
                                      x.Item == App.systemCache.ExpeditionData.ExpeditionProduct)
                                  .ToList();

                            this.HaveLot = filterData.Where(x => !string.IsNullOrEmpty(x.LotNumber)).Distinct().ToList().Count > 0 ? true : false;

                            if (App.systemCache.DeviceData.LotValidation && !string.IsNullOrEmpty(lblProduto.Text) && string.IsNullOrEmpty(lblLote.Text) && this.HaveLot) {
                                filterData = filterData = App.systemCache.ExpeditionData.ExpeditionInfoFiltered.Where(
                                                x => x.LocationOrder.Trim() == App.systemCache.ExpeditionData.ExpeditionLocal.Trim() &&
                                                x.Item.Trim() == App.systemCache.ExpeditionData.ExpeditionProduct.Trim() &&
                                                x.LotNumber.Trim() == App.systemCache.ExpeditionData.ExpeditionLot.Trim())
                                            .ToList();

                                if (filterData.Count() == 0) {
                                    this.TextBoxData.Text = string.Empty;
                                    this.lblProduto.Text = string.Empty;
                                    this.ButtonOk.IsEnabled = true;
                                    this.lblLote.Text = string.Empty;
                                    this.lblCaixa.Text = string.Empty;
                                    this.TextBoxData.Placeholder = "Introduza o produto e o lote";
                                    await DisplayAlert("Expedição", "Relação Artigo/Lote inválida", "OK");
                                    return;
                                }
                            }

                            this.lblProduto.Text = App.systemCache.ExpeditionData.ExpeditionProduct;
                            this.lblLote.Text = App.systemCache.ExpeditionData.ExpeditionLot;

                            if (!string.IsNullOrEmpty(this.lblLote.Text) || (string.IsNullOrEmpty(this.lblLote.Text) && !this.HaveLot)) {
                                this.TextBoxData.Placeholder = "Introduza a caixa";
                            }

                            this.ButtonOk.IsEnabled = true;
                            this.TextBoxData.Text = string.Empty;

                            var data = new ObservableCollection<ExpeditionModel>();

                            filterData.ForEach(x => data.Add(x));

                            App.systemCache.ExpeditionData.ExpeditionInfoFiltered = data;
                            this.ListViewExpedition.ItemsSource = data;
                            AdjustListViewSize();

                            return;
                        }
                    }
                    #endregion

                    #region entrega
                    this.lblCaixa.Text = App.systemCache.ExpeditionData.ExpeditionBox;
                    if (!string.IsNullOrEmpty(response.ErroEntrega)) {
                        this.lblCaixa.Text = string.Empty;
                        this.ButtonOk.IsEnabled = true;
                        this.TextBoxData.Text = string.Empty;
                        await DisplayAlert("Expedição", response.ErroEntrega, "OK");
                        return;
                    } else {
                        if (!string.IsNullOrEmpty(response.Erro)) {
                            this.ButtonOk.IsEnabled = true;
                            this.TextBoxData.Text = string.Empty;
                            this.lblCaixa.Text = string.Empty;
                            this.TextBoxData.Focus();

                            if (App.systemCache.ExpeditionData.FailedPacking.Any()) {
                                this.ButtonReenviarM3.IsVisible = true;
                            }

                            if (response.DataToShow != null) {
                                UpdateListView(response.DataToShow);
                            }

                            await DisplayAlert("Expedição", response.Erro, "OK");
                            return;
                        } else {
                            if (response.DataToShow != null) {
                                UpdateListView(response.DataToShow);
                            }

                            this.lblCaixa.Text = string.Empty;
                            this.ButtonOk.IsEnabled = true;
                        }
                    }
                    #endregion
                } catch (Exception ex) {
                    await DisplayAlert("Expedição", ex.Message, "OK");
                    logLogic.Insert("Erro ValidarInput:" + ex.Message, LogType.ERRO, MethodBase.GetCurrentMethod().DeclaringType.Name, App.systemCache.UserData.UserName);
                }
            }
        }

        private ExpeditionValidateData ValidateInput () {
            var model = new ExpeditionValidateData();

            try {
                #region Validar Local e Filtrar lista
                if (string.IsNullOrEmpty(this.lblLocal.Text) && this.TextBoxData.Text.Length > 0) {
                    var result = readsValidationLogic.ValidateLocalRead(this.TextBoxData.Text.Trim());

                    if (!string.IsNullOrEmpty(result.Error)) {
                        model.ErroLocal = result.Error;
                        return model;
                    } else {
                        var listByLocal = App.systemCache.ExpeditionData.ExpeditionInfoFiltered.Where(x => x.LocationOrder == result.Local).ToList();
                        model.ListByLocal = listByLocal;

                        if (listByLocal.Count() == 0) {
                            model.ErroLocal = "Local inválido para estas expedições";
                            return model;
                        }
                    }

                    App.systemCache.ExpeditionData.ExpeditionLocal = result.Local;
                    App.systemCache.ExpeditionData.ExpeditionLocalLbl = this.TextBoxData.Text.Trim();

                    return model;
                }
                #endregion

                #region Validar Produto e Lote e Filtrar Lista
                var searchProduct = 0;

                //Leitura codigo EAN13 ou ITF14 ou Code39
                if (string.IsNullOrEmpty(this.lblProduto.Text) && this.TextBoxData.Text.Length > 0) {
                    var result = readsValidationLogic.ValidateProdutoLote(this.TextBoxData.Text.Trim(), checkLot);

                    if (!string.IsNullOrEmpty(result.Error)) {
                        var item = App.systemCache.ExpeditionData.ExpeditionInfoFiltered.Where(x => x.Alias == this.TextBoxData.Text.Trim()).FirstOrDefault();

                        if (item != null) {
                            result.Item = item.Item;
                            result.Lot = item.LotNumber;
                        } else {
                            model.ErroProdutoLote = "Não conseguiu fazer parse do produto";
                            return model;
                        }
                    }

                    checkLot = result.CheckLot;

                    searchProduct = App.systemCache.ExpeditionData.ExpeditionInfoFiltered.Where(x => x.Item == result.Item).Count();

                    if (searchProduct == 0 && this.TextBoxData.Text.Length > 0) {
                        model.ErroProdutoLote = "Produto inválido para estas expedições";
                        return model;
                    }

                    //Filtro para atualizar a lista mediante leituras
                    App.systemCache.ExpeditionData.ExpeditionProduct = result.Item;
                    App.systemCache.ExpeditionData.ExpeditionLot = result.Lot;

                    return model;
                } else {
                    //Validar lote
                    if (string.IsNullOrEmpty(this.lblLote.Text) && this.TextBoxData.Text.Length > 0 && checkLot != 0) {
                        App.systemCache.ExpeditionData.ExpeditionLot = this.TextBoxData.Text;
                        return model;
                    }
                }
                #endregion

                #region Validar Entrega e Caixa e Filtrar Lista
                if (string.IsNullOrEmpty(this.lblCaixa.Text) && this.TextBoxData.Text.Length > 0) {
                    var dataTextBox = this.TextBoxData.Text.Trim();
                    var dataToFilter = dataTextBox.Split('|');

                    long entrega = 0;
                    var caixa = string.Empty;
                    int searchBox = 0;

                    if (dataToFilter.Length == 2) {
                        entrega = long.Parse(dataToFilter[0].Trim());
                        caixa = dataToFilter[1].Trim();
                        searchBox = App.systemCache.ExpeditionData.ExpeditionInfoFiltered.Where(x => x.DeliveryNumber == entrega).Count();
                    }

                    if (searchBox == 0) {
                        model.ErroEntrega = "Entrega inválida para estas expedições";
                        return model;
                    }

                    App.systemCache.ExpeditionData.ExpeditionDelivery = entrega;
                    App.systemCache.ExpeditionData.ExpeditionBox = caixa;
                    App.systemCache.ExpeditionData.ExpeditionDeliveryLbl = dataTextBox;

                    if (App.systemCache.ExpeditionData.DeliveryBoxDic.ContainsKey(App.systemCache.ExpeditionData.ExpeditionDelivery)) {
                        var checkIFExist = App.systemCache.ExpeditionData.DeliveryBoxDic[App.systemCache.ExpeditionData.ExpeditionDelivery].Where(x => x.Trim().ToLower().Contains(App.systemCache.ExpeditionData.ExpeditionBox)).Count();

                        if (checkIFExist > 0) {
                            model.ErroEntrega = "Esta caixa já foi lida nesta entrega";
                            return model;
                        }
                    }

                    var poco = ValidarCaixaIncompleta();
                    if (poco != null) {
                        Application.Current.MainPage = new ExpeditionIncompleteBox(poco.Item1, poco.Item2);
                    } else {
                        var result = expeditionLogic.ProcessReading(null, null);
                        model.DataToShow = result.Item2;

                        var itemAlias = App.systemCache.ExpeditionData.ExpeditionInfoFiltered
                            .Where(x => x.DeliveryNumber == App.systemCache.ExpeditionData.ExpeditionDelivery && x.Item == App.systemCache.ExpeditionData.ExpeditionProduct).FirstOrDefault();

                        if (!string.IsNullOrEmpty(result.Item1)) {
                            model.Erro = "Erro: " + result.Item1;
                            return model;
                        }

                        if (itemAlias != null && !string.IsNullOrEmpty(itemAlias.Alias)) {
                            Task.Run(() => {
                                var printLogic = new PrintLogic();
                                printLogic.Print(App.systemCache.DeviceData.FileNameToPrint, App.systemCache.DeviceData.PrinterName, itemAlias.Alias, itemAlias.Item, App.systemCache.UserData.UserID);
                            });
                        }
                    }
                }
                #endregion

                return model;
            } catch (Exception ex) {
                logLogic.Insert("Erro ValidateInput:" + ex.Message, LogType.ERRO, MethodBase.GetCurrentMethod().DeclaringType.Name, App.systemCache.UserData.UserName);
                model.Erro = "Erro:" + ex.Message;
                return model;
            }
        }

        private void UpdateListView (List<ExpeditionModel> dataToShow) {
            try {
                var data = new ObservableCollection<ExpeditionModel>();

                dataToShow.ForEach(x => data.Add(x));
                this.ListViewExpedition.ItemsSource = data;
                App.systemCache.ExpeditionData.ExpeditionInfoFiltered = data;
                AdjustListViewSize();

                this.lblCaixa.Text = string.Empty;
                this.lblLote.Text = string.Empty;
                this.lblProduto.Text = string.Empty;
                this.TextBoxData.Text = string.Empty;
                this.ButtonOk.IsEnabled = true;
                this.TextBoxData.Placeholder = "Introduza o produto e o lote";

                if (dataToShow.Count > 0 && dataToShow.Where(x => x.LocationOrder == App.systemCache.ExpeditionData.ExpeditionLocal).Count() == 0) {
                    this.lblLocal.Text = string.Empty;
                    App.systemCache.ExpeditionData.ExpeditionLocal = string.Empty;
                    App.systemCache.ExpeditionData.ExpeditionLocalLbl = string.Empty;
                    this.TextBoxData.Placeholder = "Introduza o local";
                }

                this.TextBoxData.Focus();

                if (App.systemCache.ExpeditionData.FailedPacking.Any()) {
                    this.ButtonReenviarM3.IsVisible = true;
                    return;
                }

                if (data.Count() == 0) {
                    App.systemCache.ExpeditionClearAllData();
                    this.lblLocal.Text = string.Empty;
                    this.lblLote.Text = string.Empty;
                    this.lblProduto.Text = string.Empty;
                    this.lblCaixa.Text = string.Empty;
                    this.TextBoxData.Placeholder = "Introduza o local";

                    Application.Current.MainPage = new ExpeditionMenu();
                }
            } catch (Exception ex) {
                DisplayAlert("Expedição", ex.Message, "OK");
                logLogic.Insert("Erro UpdateListView:" + ex.Message, LogType.ERRO, MethodBase.GetCurrentMethod().DeclaringType.Name, App.systemCache.UserData.UserName);
            }
        }

        private Tuple<long, decimal> ValidarCaixaIncompleta () {
            var info = App.systemCache.ExpeditionData.ExpeditionInfoFiltered.Where(x => x.DeliveryNumber == App.systemCache.ExpeditionData.ExpeditionDelivery).FirstOrDefault();

            if (info != null && info.MissingQty % info.MaximumQty != 0 && info.MissingQty < info.MaximumQty) {
                return new Tuple<long, decimal>(info.DeliveryNumber, info.MissingQty);
            }

            return null;
        }

        private void AdjustListViewSize () {
            try {
                var rowsCount = App.systemCache.ExpeditionData.ExpeditionInfo.Count;
                var columnsCount = 9;

                this.ListViewExpedition.HeightRequest = rowSize;
                this.ListViewExpedition.WidthRequest = (columnsCount * columnSize);
            } catch (Exception ex) {
                DisplayAlert("Expedição", ex.Message, "OK");
                logLogic.Insert("Erro AdjustListViewSize:" + ex.Message, LogType.ERRO, MethodBase.GetCurrentMethod().DeclaringType.Name, App.systemCache.UserData.UserName);
            }
        }

        private void TextBoxData_TextChanged (object sender, TextChangedEventArgs e) {
            if (this.TextBoxData.Text.Trim().Length > 0 &&
                this.TextBoxData.Text.Trim().Length - this.TextBoxLength != 1 &&
                this.TextBoxData.Text.Trim().Length > this.TextBoxLength) {
                ValidarInput();
                this.TextBoxData.Focus();
            } else {
                this.TextBoxLength = this.TextBoxData.Text.Trim().Length;

                if (this.TextBoxLength > 0) {
                    this.ButtonOk.IsEnabled = true;
                } else {
                    this.ButtonOk.IsEnabled = false;
                }
            }
        }

        private void ButtonReenviarM3_Clicked (object sender, EventArgs e) {
            try {
                if (!App.systemCache.ExpeditionData.FailedPacking.Any()) {
                    ButtonReenviarM3.IsVisible = false;
                    DisplayAlert("Expedição", "Não há dados para enviar para o M3", "OK");
                    return;
                }

                expeditionLogic.ProcessFailedReading();

                if (App.systemCache.ExpeditionData.FailedPacking.Any()) {
                    DisplayAlert("Expedição", "Falhou ao reenviar para o M3. Contacte o administrador.", "OK");
                    return;
                }
            } catch (Exception ex) {
                logLogic.Insert("Erro Reenviar: " + ex.Message, LogType.ERRO, MethodBase.GetCurrentMethod().DeclaringType.Name, App.systemCache.UserData.UserName);
                DisplayAlert("Expedição", "Ocorreu um erro de processamento. Contacte o administrador.", "OK");
            }
        }

        private async void ContentPage_Appearing (object sender, EventArgs e) {
            await Task.Delay(600);
            this.TextBoxData.Focus();
        }
    }
}