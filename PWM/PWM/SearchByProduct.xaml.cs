﻿using Acr.UserDialogs;
using Newtonsoft.Json;
using PWM.Logic;
using PWM.Models.Log;
using PWM.Models.Reads;
using PWM.Models.Search;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PWM {
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SearchByProduct : ContentPage {
        #region properties
        public int TextBoxLength { get; set; }
        public string WarehouseFilter { get; set; }
        public int rowSize = 50;
        public int columnSize = 100;
        public string read { get; set; }
        public string Menu { get; set; }
        #endregion

        #region dependencies
        M3Logic m3Logic = new M3Logic();
        LogLogic logLogic = new LogLogic();
        #endregion

        public SearchByProduct (string menu) {
            InitializeComponent();
            this.Menu = menu;
            this.ButtonFilter.IsEnabled = false;
            this.ButtonFilter.Text = "Ver só armazém actual";
            this.ListViewExpedition.IsVisible = false;
        }

        private void ButtonFilter_Clicked (object sender, EventArgs e) {
            if (string.IsNullOrEmpty(this.WarehouseFilter)) {
                this.WarehouseFilter = App.systemCache.DeviceData.Warehouse;
                this.ButtonFilter.Text = "Ver todos os armazéns";
            } else {
                this.WarehouseFilter = string.Empty;
                this.ButtonFilter.Text = "Ver só armazém actual";
            }

            FillData();
        }

        private void TextBoxFilter_Completed (object sender, EventArgs e) {
            FillData();
        }

        private void TextBoxFilter_TextChanged (object sender, TextChangedEventArgs e) {
            if (this.TextBoxFilter.Text.Trim().Length > 0 &&
             this.TextBoxFilter.Text.Trim().Length - this.TextBoxLength != 1 &&
             this.TextBoxFilter.Text.Trim().Length > this.TextBoxLength) {
                FillData();
                this.TextBoxFilter.Focus();
            } else {
                this.TextBoxLength = this.TextBoxFilter.Text.Trim().Length;

                if (this.TextBoxLength > 0) {
                    this.ButtonOk.IsEnabled = true;
                } else {
                    this.ButtonOk.IsEnabled = false;
                }
            }
        }

        private void TextBoxFilter_Unfocused (object sender, FocusEventArgs e) {
            this.TextBoxFilter.Focus();
        }

        private void ButtonOk_Clicked (object sender, EventArgs e) {
            FillData();
        }

        private async void FillData () {
            using (UserDialogs.Instance.Loading("Loading...", null, null, true)) {
                try {
                    var response = await Task.Run(() => GetInfo());

                    if (!string.IsNullOrEmpty(response.Item1)) {
                        this.TextBoxFilter.Text = string.Empty;
                        this.ButtonFilter.Text = "Ver só armazém actual";
                        this.labelHeader.Text = string.Empty;
                        await DisplayAlert("SearchByProduct", response.Item1, "OK");
                        return;
                    } else {
                        this.labelHeader.Text = response.Item2;
                        this.ButtonFilter.IsEnabled = true;
                        this.TextBoxFilter.Text = string.Empty;

                        var data = new ObservableCollection<Models.Search.SearchByProductModel>();
                        response.Item3.ForEach(x => data.Add(x));
                        AdjustView(data);
                    }
                } catch (Exception ex) {
                    await DisplayAlert("Pesquisar por produto", ex.Message, "OK");
                    logLogic.Insert("Erro FillData:" + ex.Message, LogType.ERRO, MethodBase.GetCurrentMethod().DeclaringType.Name, App.systemCache.UserData.UserName);
                }
            }
        }

        private Tuple<string, string, List<SearchByProductModel>> GetInfo () {
            if (this.TextBoxFilter.Text != "") {
                try {
                    var poco = JsonConvert.DeserializeObject<ReadProduct>(this.TextBoxFilter.Text.Trim());
                    this.read = poco.lblItemID.Trim();
                } catch (Exception) {
                    var alias = this.TextBoxFilter.Text.Trim();
                    var itemPoco = App.systemCache.ExpeditionData.ExpeditionInfoFiltered.Where(x => x.Alias == alias).FirstOrDefault();

                    if (itemPoco != null) {
                        this.read = itemPoco.Item;
                    } else {
                        return new Tuple<string, string, List<SearchByProductModel>>("Não conseguiu fazer parse do produto", string.Empty, null);
                    }
                }
            }

            try {
                var m3Data = m3Logic.SearchByProduct(App.systemCache.DeviceData.RealEnvironment, App.systemCache.DeviceData.Company, App.systemCache.DeviceData.Facility, this.read, this.WarehouseFilter);

                if (!m3Data.Any()) {
                    return new Tuple<string, string, List<SearchByProductModel>>("Não existe informação para este produto", string.Empty, null);
                }

                var item = m3Data.FirstOrDefault();
                return new Tuple<string, string, List<SearchByProductModel>>(string.Empty, item.Item + " - " + item.ItemDescription, m3Data);
            } catch (Exception ex) {
                logLogic.Insert(ex.Message, LogType.ERRO, MethodBase.GetCurrentMethod().DeclaringType.Name, App.systemCache.UserData.UserName);
                return new Tuple<string, string, List<SearchByProductModel>>("Erro:" + ex.Message, string.Empty, null);
            }
        }

        private void AdjustView (ObservableCollection<Models.Search.SearchByProductModel> data) {
            this.ListViewExpedition.ItemsSource = data;

            var rowsCount = data.Count;
            var columnsCount = 8;

            this.ListViewExpedition.HeightRequest = rowSize;
            this.ListViewExpedition.WidthRequest = (columnsCount * columnSize);
            this.ListViewExpedition.IsVisible = true;
        }

        private void TapGestureBackButton_Tapped (object sender, EventArgs e) {
            var page = Activator.CreateInstance(Type.GetType(this.Menu)) as Page;
            Application.Current.MainPage = page;
        }

        private void ContentPage_Appearing (object sender, EventArgs e) {
            this.TextBoxFilter.Focus();
        }
    }
}