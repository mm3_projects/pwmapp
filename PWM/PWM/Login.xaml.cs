﻿using Acr.UserDialogs;
using Newtonsoft.Json;
using Plugin.DeviceInfo;
using PWM.Core;
using PWM.Logic;
using PWM.Models.Device;
using PWM.Models.Log;
using PWM.Models.PWMConfigs;
using PWM.Models.User;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PWM {
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Login : ContentPage {
        #region dependencies
        UserLogic userLoginLogic = new UserLogic();
        DeviceConfigLogic deviceConfigLogic = new DeviceConfigLogic();
        MenuPermissionViewLogic menuPermissionLogic = new MenuPermissionViewLogic();
        LogLogic logLogic = new LogLogic();
        #endregion

        #region properties
        public DataTable DeviceConfigs { get; set; }
        public string deviceSerial { get; set; }
        #endregion

        public Login () {
            InitializeComponent();

            try {
                FillUsersDropDown();
                FillConfigs();
            } catch { }
        }

        private void FillUsersDropDown () {
            var pocos = userLoginLogic.GetAll();

            if (pocos != null) {
                usersDropDown.ItemsSource = pocos.Rows.OfType<DataRow>()
                    .Select(x => x["UserName"].ToString())
                    .ToList();

                usersDropDown.SelectedIndex = 0;
            }
        }

        private void FillConfigs () {
            this.deviceSerial = CrossDeviceInfo.Current.Id;
            var configs = deviceConfigLogic.GetAll(this.deviceSerial);

            if (configs == null || configs.Rows.Count == 0) {
                DisplayAlert("Login", "Este device não tem configurações. Contacte o administrador.", "OK");
                LoginButton.IsEnabled = false;
                txtPassword.IsEnabled = false;
                usersDropDown.IsEnabled = false;
                return;
            }

            var configsList = configs.Rows.OfType<DataRow>().ToList();
            var realEnvironmentConfig = configsList
                .Where(x => x["ConfigName"].ToString() == ConfigEnum.RealEnvironmentConfig)
                .Select(x => x["ConfigValue"].ToString())
                .FirstOrDefault();

            var realEnvironment = bool.Parse(realEnvironmentConfig);

            if (realEnvironment) {
                PanelLogin.BackgroundColor = Color.FromHex("#3897F0");
            } else {
                PanelLogin.BackgroundColor = Color.FromHex("#ff5050");
            }

            this.DeviceConfigs = configs;
        }

        private async void LoginButton_Clicked (object sender, EventArgs e) {
            using (UserDialogs.Instance.Loading("Loading...", null, null, true)) {
                LoginButton.IsEnabled = false;
                var response = await Task.Run(() => ValidateLogin(usersDropDown.SelectedItem.ToString(), txtPassword.Text));

                if (!string.IsNullOrEmpty(response)) {
                    await DisplayAlert("Login", response, "OK");
                    LoginButton.IsEnabled = true;
                    return;
                }

                Application.Current.MainPage = new MenuMenu();
            }
        }

        private string ValidateLogin (string user, string password) {
            DataTable userData;

            #region validateUser
            try {
                if (usersDropDown.SelectedItem == null || string.IsNullOrEmpty(txtPassword.Text)) {
                    return "Preencha as credenciais";
                }

                userData = userLoginLogic.CheckLogin(user, password);

                if (userData.Rows.Count == 0) {
                    return "Credenciais Inválidas!";
                }
            } catch (Exception ex) {
                logLogic.Insert(ex.Message, LogType.ERRO, MethodBase.GetCurrentMethod().DeclaringType.Name, user);
                return "Erro ao validar login. Contacte o administrador.";
            }
            #endregion

            #region fillConfigs
            try {
                var pwmConfigs = APIDeserializedDataLogic.GetConfigsData();
                var configsList = this.DeviceConfigs.Rows.OfType<DataRow>().ToList();

                App.systemCache = new SystemCache() {
                    UserData = new UserModelCache() {
                        Permissions = menuPermissionLogic.GetAll(userData.Rows[0]["UserID"].ToString()),
                        UserID = long.Parse(userData.Rows[0]["UserID"].ToString()),
                        UserName = userData.Rows[0]["UserName"].ToString()
                    },
                    DeviceData = new DeviceModelCache() {
                        Company = configsList
                                .Where(x => x["ConfigName"].ToString() == ConfigEnum.CompanyConfig)
                                .Select(x => x["ConfigValue"].ToString())
                                .FirstOrDefault(),
                        ConfigsID = configsList.ToDictionary(x => x["ConfigName"].ToString(), x => long.Parse(x["ConfigID"].ToString())),
                        DeviceID = long.Parse(configsList.Select(x => x["DeviceID"].ToString()).FirstOrDefault()),
                        DeviceSerial = this.deviceSerial,
                        Division = configsList
                                .Where(x => x["ConfigName"].ToString() == ConfigEnum.DivisonConfig)
                                .Select(x => x["ConfigValue"].ToString())
                                .FirstOrDefault(),
                        Facility = configsList
                                .Where(x => x["ConfigName"].ToString() == ConfigEnum.FacilityConfig)
                                .Select(x => x["ConfigValue"].ToString())
                                .FirstOrDefault(),
                        FileNameToPrint = configsList
                                .Where(x => x["ConfigName"].ToString() == ConfigEnum.FileNameToPrintConfig)
                                .Select(x => x["ConfigValue"].ToString())
                                .FirstOrDefault(),
                        PrinterName = configsList
                                .Where(x => x["ConfigName"].ToString() == ConfigEnum.PrinterNameConfig)
                                .Select(x => x["ConfigValue"].ToString())
                                .FirstOrDefault(),
                        RealEnvironment = bool.Parse(configsList
                                .Where(x => x["ConfigName"].ToString() == ConfigEnum.RealEnvironmentConfig)
                                .Select(x => x["ConfigValue"].ToString())
                                .FirstOrDefault()),
                        Warehouse = configsList
                                .Where(x => x["ConfigName"].ToString() == ConfigEnum.WarehouseConfig)
                                .Select(x => x["ConfigValue"].ToString())
                                .FirstOrDefault(),
                        ExpeditionToLocation = configsList
                                .Where(x => x["ConfigName"].ToString() == ConfigEnum.ExpeditionToLocationConfig)
                                .Select(x => x["ConfigValue"].ToString())
                                .FirstOrDefault(),
                        LotValidation = bool.Parse(configsList
                                .Where(x => x["ConfigName"].ToString() == ConfigEnum.LotValidationConfig)
                                .Select(x => x["ConfigValue"].ToString())
                                .FirstOrDefault())
                    },
                    GlobalConfigsCache = new GlobalConfigsCache() {
                        Responsible = pwmConfigs[ConfigEnum.ResponsibleConfig],
                        MoveStockLocations = JsonConvert.DeserializeObject<List<string>>(pwmConfigs[ConfigEnum.MoveStockLocationsConfig]),
                        IgnoreAlias = JsonConvert.DeserializeObject<List<string>>(pwmConfigs[ConfigEnum.IgnoreClientAliasConfig])
                    }
                };

                #region log
                var ambiente = App.systemCache.DeviceData.RealEnvironment == true ? "Real" : "Teste";
                var versão = "v" + DependencyService.Get<IAppVersion>().GetVersionNumber();
                var device = App.systemCache.DeviceData.DeviceSerial;
                var message = string.Format("Login Feito em ambiente {0} na {1} com o device {2}", ambiente, versão, device);
                logLogic.Insert(message, LogType.INFO, MethodBase.GetCurrentMethod().DeclaringType.Name, App.systemCache.UserData.UserName);
                #endregion

                return string.Empty;
            } catch (Exception ex) {
                logLogic.Insert(ex.Message, LogType.ERRO, MethodBase.GetCurrentMethod().DeclaringType.Name, App.systemCache.UserData.UserName);
                return "Configurações/Permissoes erradas. Contacte o administrador.";
            }
            #endregion
        }
    }
}