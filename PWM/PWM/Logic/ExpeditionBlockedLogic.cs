﻿using PWM.Core;
using PWM.Models.Requests;
using System.Collections.Generic;
using System.Data;

namespace PWM.Logic {
    public class ExpeditionBlockedLogic {
        public DataTable Get (List<string> expeditionNumbers) {
            return APIDeserializedDataLogic.GetData(new GeneralRequestParameters() {
                Query = Repository.GetExpeditionBlocked(expeditionNumbers)
            });
        }


        public void Insert (long userID, List<string> expeditionNumber, long deviceID) {
            var queryAll = string.Empty;

            foreach (var expedition in expeditionNumber) {
                queryAll += Repository.InsertExpeditionBlocked(userID, expedition, deviceID) + ";";
            }

            APIDeserializedDataLogic.ExecuteData(new GeneralRequestParameters() {
                Query = queryAll
            });
        }

        public void Delete (List<string> expeditionNumbers, long userID, long deviceID) {
            APIDeserializedDataLogic.ExecuteData(new GeneralRequestParameters() {
                Query = Repository.RemoveExpeditionBlocked(expeditionNumbers, userID, deviceID)
            });
        }
    }
}
