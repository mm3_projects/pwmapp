﻿using Newtonsoft.Json;
using PWM.Core.PolisportCore;
using PWM.Models.Requests;
using System;
using System.Collections.Generic;
using System.Data;
using System.Net;
using System.Text;

namespace PWM.Logic {
    public static class APIDeserializedDataLogic {
        public static System.Data.DataTable GetData (GeneralRequestParameters parameters) {
            var request = (HttpWebResponse)EconnetorInvoke.EndpoinConnector(EconnetorInvoke.EnderecosPedidos.GeneralGetData, parameters);
            var response = string.Empty;


            var encoding = UTF8Encoding.UTF8;
            using (var reader = new System.IO.StreamReader(request.GetResponseStream(), encoding)) {
                response = reader.ReadToEnd();
            }

            if (request.StatusCode == HttpStatusCode.OK) {
                return JsonConvert.DeserializeObject<DataTable>(response);
            } else {
                throw new Exception(response);
            }
        }

        public static Dictionary<string, string> GetConfigsData () {
            var request = (HttpWebResponse)EconnetorInvoke.EndpoinConnector(EconnetorInvoke.EnderecosPedidos.GeneralGetConfigs, "PWM");
            var response = string.Empty;

            var encoding = UTF8Encoding.UTF8;
            using (var reader = new System.IO.StreamReader(request.GetResponseStream(), encoding)) {
                response = reader.ReadToEnd();
            }

            if (request.StatusCode == HttpStatusCode.OK) {
                return JsonConvert.DeserializeObject<Dictionary<string, string>>(response);
            } else {
                throw new Exception(response);
            }
        }

        public static void ExecuteData (GeneralRequestParameters parameters) {
            var request = (HttpWebResponse)EconnetorInvoke.EndpoinConnector(EconnetorInvoke.EnderecosPedidos.GeneralExecuteData, parameters);

            if (request.StatusCode != HttpStatusCode.OK) {
                var encoding = UTF8Encoding.UTF8;
                var response = string.Empty;

                using (var reader = new System.IO.StreamReader(request.GetResponseStream(), encoding)) {
                    response = reader.ReadToEnd();
                }

                throw new Exception(response);
            }
        }
    }
}
