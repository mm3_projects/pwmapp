﻿using PWM.Core;
using PWM.Models.Requests;
using System.Data;

namespace PWM.Logic {
    public class UserLogic {
        public DataTable CheckLogin (string user, string password) {
            var newPassword = GlobalFunctions.Encryption(password);

            return APIDeserializedDataLogic.GetData(new GeneralRequestParameters() {
                Query = Repository.GetByUserAndPassword(user, newPassword)
            });
        }

        public DataTable GetAll () {
            return APIDeserializedDataLogic.GetData(new GeneralRequestParameters() {
                Query = Repository.GetAllUsers()
            });
        }
    }
}
