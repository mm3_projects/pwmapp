﻿using PWM.Core;
using PWM.Models.Log;
using PWM.Models.Requests;

namespace PWM.Logic {
    public class LogLogic {
        public void Insert (string log, LogType logType, string menu, string user) {
            APIDeserializedDataLogic.ExecuteData(new GeneralRequestParameters() {
                Query = Repository.InsertLog(log, logType.ToString(), menu, user)
            });
        }
    }
}
