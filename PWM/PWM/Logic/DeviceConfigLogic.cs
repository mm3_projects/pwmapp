﻿using PWM.Core;
using PWM.Models.Device;
using PWM.Models.Requests;
using System.Collections.Generic;
using System.Data;

namespace PWM.Logic {
    public class DeviceConfigLogic {
        public DataTable GetAll (string serial) {
            return APIDeserializedDataLogic.GetData(new GeneralRequestParameters() {
                Query = Repository.GetDeviceConfigs(serial)
            });
        }

        public void Update (List<UpdateConfigModel> configs) {
            var query = string.Empty;

            foreach (var config in configs) {
                query += Repository.UpdateWarehouse(config.ConfigValue, config.ConfigID, config.DeviceID) + ";";
            }

            APIDeserializedDataLogic.ExecuteData(new GeneralRequestParameters() {
                Query = query
            });
        }
    }
}
