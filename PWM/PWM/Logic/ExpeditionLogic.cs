﻿using Newtonsoft.Json;
using PWM.Core;
using PWM.Core.PolisportCore;
using PWM.Models;
using PWM.Models.Expedition;
using PWM.Models.Log;
using PWM.Models.Mittra;
using PWM.Models.Requests;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;

namespace PWM.Logic {
    public class ExpeditionLogic {
        #region dependencies
        LogLogic logLogic = new LogLogic();
        MittraLogic mittraLogic = new MittraLogic();
        #endregion

        public Tuple<string, List<ExpeditionModel>> ProcessReading (string tipoCaixa, int? packedQty, bool incompleteBox = false) {
            try {
                var poco = App.systemCache.ExpeditionData.ExpeditionInfoFiltered
                    .Where(x => x.DeliveryNumber == App.systemCache.ExpeditionData.ExpeditionDelivery
                            && x.Item.Trim() == App.systemCache.ExpeditionData.ExpeditionProduct.Trim())
                    .FirstOrDefault();

                var pocoAddLine = new MWS420() {
                    Company = App.systemCache.DeviceData.Company,
                    Container = poco.Container,
                    DeliveryNumber = App.systemCache.ExpeditionData.ExpeditionDelivery,
                    Divi = App.systemCache.DeviceData.Division,
                    ItemNumber = App.systemCache.ExpeditionData.ExpeditionProduct,
                    LineSuffix = poco.LineSuffix,
                    Location = poco.LocationOrder,
                    LotNumber = poco.LotNumber,
                    OrderLine = poco.OrderLine,
                    OrderNumber = poco.OrderNumber,
                    OrderOperation = null,
                    PackageNumber = App.systemCache.ExpeditionData.ExpeditionBox,
                    Packaging = string.IsNullOrEmpty(tipoCaixa) ? poco.Packaging : tipoCaixa,
                    PackedQuantity = packedQty.HasValue ? packedQty.Value : poco.MaximumQty,
                    PickingListSuffix = poco.PickingListSuffix,
                    StockTransactionType = 31,
                    Warehouse = App.systemCache.DeviceData.Warehouse,
                    YesNo = null
                };

                var pocoPack = new NWS422() {
                    Company = App.systemCache.DeviceData.Company,
                    Divi = App.systemCache.DeviceData.Division,
                    Quantity = packedQty.HasValue ? packedQty.Value : poco.MaximumQty,
                    Location = App.systemCache.DeviceData.ExpeditionToLocation,
                    ReportingNumber = poco.ReportingNumber
                };

                var pocoPacking = new PackingModel() {
                    AddLine = pocoAddLine,
                    Pack = pocoPack
                };

                var transactionDate = DateTime.Now;
                var result = PackEndpointCall(pocoPacking);

                if (string.IsNullOrEmpty(result.Item1) && string.IsNullOrEmpty(result.Item2)) {
                    var mitraPoco = new MittraPoco();
                    try {
                        mitraPoco = new MittraPoco() {
                            CREATEDATE = DateTime.Now,
                            MTBANO = pocoPacking.AddLine.LotNumber,
                            MTCONO = decimal.Parse(pocoPacking.AddLine.Company),
                            MTITNO = pocoPacking.AddLine.ItemNumber,
                            MTPLSX = pocoPacking.AddLine.PickingListSuffix,
                            MTRESP = App.systemCache.UserData.UserName,
                            MTRIDI = pocoPacking.AddLine.DeliveryNumber,
                            MTRIDL = pocoPacking.AddLine.OrderLine,
                            MTRIDN = pocoPacking.AddLine.OrderNumber.ToString(),
                            MTRIDO = pocoPacking.AddLine.OrderOperation,
                            MTSLTP = poco.StockZone,
                            MTTRQT = pocoPacking.AddLine.PackedQuantity,
                            MTTTID = "MVL",
                            MTTTYP = poco.StockTransactionType,
                            MTWHLO = pocoPacking.AddLine.Warehouse,
                            MTWHSL = pocoPacking.AddLine.Location,
                            TRANSACTIONDATE = transactionDate
                        };

                        mittraLogic.Insert(mitraPoco);
                    } catch (Exception ex) {
                        var message = "ERROR:" + ex.ToString() + "; POCO:" + JsonConvert.SerializeObject(mitraPoco);
                        EconnetorInvoke.EndpoinConnector(EconnetorInvoke.EnderecosPedidos.SendEmail, new EmailDataModel() {
                            Assunto = "PWM - Erro ao guardar - MITTRA",
                            Para = new string[] { SystemConfigs.EmailErros },
                            CC = new string[] { },
                            Body = message
                        });
                    }

                    if (!incompleteBox) {
                        if (App.systemCache.ExpeditionData.DeliveryBoxDic.ContainsKey(App.systemCache.ExpeditionData.ExpeditionDelivery)) {
                            App.systemCache.ExpeditionData.DeliveryBoxDic[App.systemCache.ExpeditionData.ExpeditionDelivery].Add(App.systemCache.ExpeditionData.ExpeditionBox);
                        } else {
                            App.systemCache.ExpeditionData.DeliveryBoxDic.Add(App.systemCache.ExpeditionData.ExpeditionDelivery, new List<string> { App.systemCache.ExpeditionData.ExpeditionBox });
                        }
                    }

                    var dataToUpdate = App.systemCache.ExpeditionData.ExpeditionInfoFiltered.ToList();

                    foreach (var pocoToUpdate in dataToUpdate) {
                        if (pocoToUpdate.DeliveryNumber == App.systemCache.ExpeditionData.ExpeditionDelivery && pocoToUpdate.Item == App.systemCache.ExpeditionData.ExpeditionProduct) {
                            if (packedQty.HasValue) {
                                pocoToUpdate.MissingQty = pocoToUpdate.MissingQty - packedQty.Value;
                            } else {
                                pocoToUpdate.MissingQty = pocoToUpdate.MissingQty - pocoToUpdate.MaximumQty;
                            }

                            break;
                        }
                    }

                    dataToUpdate = App.systemCache.ExpeditionData.ExpeditionInfo.Where(x => x.MissingQty > 0 && x.LocationOrder == App.systemCache.ExpeditionData.ExpeditionLocal).ToList();

                    if (dataToUpdate.Count() == 0) {
                        dataToUpdate = App.systemCache.ExpeditionData.ExpeditionInfo.Where(x => x.MissingQty > 0).ToList();
                    }

                    return new Tuple<string, List<ExpeditionModel>>(string.Empty, dataToUpdate);
                } else {

                    if (!string.IsNullOrEmpty(result.Item1)) {
                        pocoPacking.AddLine = null;
                    }

                    if (!string.IsNullOrEmpty(result.Item2)) {
                        pocoPacking.Pack = null;
                    }

                    var joinMessage = string.Empty;
                    if (!string.IsNullOrEmpty(result.Item1)) {
                        joinMessage = result.Item1;
                    }

                    if (!string.IsNullOrEmpty(result.Item2)) {
                        joinMessage = string.IsNullOrEmpty(joinMessage) ? result.Item2 : ";" + result.Item2;
                    }

                    App.systemCache.ExpeditionData.FailedPacking.Add(pocoPacking);
                    return new Tuple<string, List<ExpeditionModel>>(joinMessage, null);
                }
            } catch (Exception ex) {
                logLogic.Insert(ex.Message, LogType.ERRO, MethodBase.GetCurrentMethod().DeclaringType.Name, App.systemCache.UserData.UserName);
                return new Tuple<string, List<ExpeditionModel>>(ex.Message, null);
            }
        }

        public void ProcessFailedReading () {
            var listFailed = new List<PackingModel>();
            foreach (var pack in App.systemCache.ExpeditionData.FailedPacking) {
                var packResult = PackEndpointCall(pack);

                if (!string.IsNullOrEmpty(packResult.Item1) && !string.IsNullOrEmpty(packResult.Item2)) {
                    listFailed.Add(pack);
                }
            }

            App.systemCache.ExpeditionData.FailedPacking = App.systemCache.ExpeditionData.FailedPacking.ToList().Except(listFailed).ToList();
        }

        private Tuple<string, string> PackEndpointCall (PackingModel poco) {
            try {
                var webResponse = (HttpWebResponse)EconnetorInvoke.EndpoinConnector(EconnetorInvoke.EnderecosPedidos.Pack, poco);
                var response = string.Empty;
                var encoding = Encoding.UTF8;

                using (var reader = new StreamReader(webResponse.GetResponseStream(), encoding)) {
                    response = reader.ReadToEnd();
                }

                if (webResponse.StatusCode == HttpStatusCode.OK) {
                    var message = "EndPoint Call -> AddLine: " +
                        JsonConvert.SerializeObject(poco.AddLine) +
                        "; Pack:" + JsonConvert.SerializeObject(poco.Pack) +
                        "; Response:" + response;

                    logLogic.Insert(message, LogType.INFO, MethodBase.GetCurrentMethod().DeclaringType.Name, App.systemCache.UserData.UserName);
                    return JsonConvert.DeserializeObject<Tuple<string, string>>(response);
                } else {
                    logLogic.Insert(response, LogType.ERRO, MethodBase.GetCurrentMethod().DeclaringType.Name, App.systemCache.UserData.UserName);
                    var message = "PackEndpointCall Error: " + response;
                    return new Tuple<string, string>(message, message);
                }
            } catch (Exception ex) {
                logLogic.Insert(ex.Message, LogType.ERRO, MethodBase.GetCurrentMethod().DeclaringType.Name, App.systemCache.UserData.UserName);
                var message = "PackEndpointCall Error: " + ex.Message;
                return new Tuple<string, string>(message, message);
            }
        }
    }
}
