﻿using PWM.Core;
using System;
using System.Reflection;
using Xamarin.Forms;

namespace PWM.Logic {
    public class PrintLogic {
        public string Print (string filename, string printerName, string alias, string produto, long userID) {
            try {
                var bluetoothPrinter = DependencyService.Get<IBluetoothPrinter>();

                var assembly = typeof(PrintLogic).GetTypeInfo().Assembly;
                var file = assembly.GetManifestResourceStream(filename);
                var text = string.Empty;

                using (var reader = new System.IO.StreamReader(file)) {
                    text = reader.ReadToEnd();
                }

                text = text.Replace("<@PRODUCT_CODE>", produto);
                text = text.Replace("<@CUSTOMER_ALIAS>", alias);
                text = text.Replace("<@RECEIVING_NUMBER>", "");
                text = text.Replace("<@USER>", userID.ToString());

                var dateTime = DateTime.Now;
                var year = dateTime.Year.ToString().PadLeft(4, '0');
                var month = dateTime.Month.ToString().PadLeft(2, '0');
                var day = dateTime.Day.ToString().PadLeft(2, '0');
                var time = dateTime.ToShortTimeString();

                text = text.Replace("<@TIMESTAMP>", year + "-" + month + "-" + day + " " + time);

                return bluetoothPrinter.Print(printerName, text);
            } catch (Exception ex) {
                return ex.Message;
            }
        }
    }
}
