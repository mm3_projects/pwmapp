﻿using Newtonsoft.Json;
using PWM.Models.Reads;
using System;

namespace PWM.Logic {
    public class ReadsValidationLogic {
        #region dependencies
        M3Logic m3Logic = new M3Logic();
        #endregion

        public ValidateItemLot ValidateProdutoLote (string itemRead, int checkLot) {
            var item = string.Empty;
            var lot = string.Empty;
            var description = string.Empty;

            //Leitura MITPOP
            var data = m3Logic.GetAliasMITPOP(App.systemCache.DeviceData.RealEnvironment, itemRead);
            if (data != null && data.Rows.Count > 0) {
                item = data.Rows[0]["Item"].ToString();
                checkLot = int.Parse(data.Rows[0]["Lot"].ToString());
                description = data.Rows[0]["Description"].ToString();
            }

            //Leitura MITMAS
            if (string.IsNullOrEmpty(item)) {
                data = m3Logic.GetAliasMITMAS(App.systemCache.DeviceData.RealEnvironment, itemRead);
                if (data != null && data.Rows.Count > 0) {
                    item = data.Rows[0]["Item"].ToString();
                    checkLot = int.Parse(data.Rows[0]["Lot"].ToString());
                    description = data.Rows[0]["Description"].ToString();
                }
            }

            //Leitura QRCode JSON
            if (string.IsNullOrEmpty(item) && itemRead.Length > 0) {
                try {
                    var poco = JsonConvert.DeserializeObject<ReadProduct>(itemRead.Trim());
                    item = poco.lblItemID.Trim();
                    lot = poco.lblLotID == null ? string.Empty : poco.lblLotID.Trim();

                    data = m3Logic.GetAliasMITMAS(App.systemCache.DeviceData.RealEnvironment, itemRead);
                    if (data != null && data.Rows.Count > 0) {
                        description = data.Rows[0]["Description"].ToString();
                    }
                } catch (Exception) {
                    return new ValidateItemLot() { Item = item, Lot = lot, Description = description, CheckLot = checkLot, Error = "Não conseguiu fazer parse do produto" };
                }
            }

            return new ValidateItemLot() { Item = item, Lot = lot, Description = description, CheckLot = checkLot, Error = string.Empty };
        }

        public ValidateLocal ValidateLocalRead (string localText) {
            var localSplit = localText.Trim().Split('|');

            if (localSplit.Length == 2) {
                if (localSplit[0].ToLower().Trim() != App.systemCache.DeviceData.Warehouse.ToLower().Trim()) {
                    return new ValidateLocal() { Warehouse = localSplit[0], Local = localSplit[1], Error = "O local não está associado ao armazém onde se encontra" };
                }

                return new ValidateLocal() { Warehouse = localSplit[0].Trim(), Local = localSplit[1].Trim(), Error = string.Empty };
            } else {
                return new ValidateLocal() { Error = "Local inválido" };
            }
        }
    }
}
