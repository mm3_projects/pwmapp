﻿using PWM.Core;
using PWM.Models.Requests;
using System.Data;

namespace PWM.Logic {
    public class MenuPermissionViewLogic {
        public DataTable GetAll (string userID) {
            return APIDeserializedDataLogic.GetData(new GeneralRequestParameters() {
                Query = Repository.GetUserPermissions(userID)
            });
        }
    }
}
