﻿using PWM.Core;
using PWM.Models.Mittra;
using PWM.Models.Requests;

namespace PWM.Logic {
    public class MittraLogic {
        public void Insert (MittraPoco poco) {
            APIDeserializedDataLogic.ExecuteData(new GeneralRequestParameters() {
                Query = Repository.InsertMittra(poco)
            });
        }
    }
}
