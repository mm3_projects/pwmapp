﻿using Newtonsoft.Json;
using PWM.Core.PolisportCore;
using PWM.Models;
using PWM.Models.Log;
using PWM.Models.MoveLocation;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;

namespace PWM.Logic {
    public class MoveLocationLogic {
        #region dependencies
        LogLogic logLogic = new LogLogic();
        #endregion

        public string Process (List<MoveLocationPoco> list) {
            try {
                var message = string.Empty;
                var fixLocations = list.Where(x => App.systemCache.GlobalConfigsCache.MoveStockLocations.Contains(x.Local)).ToList();
                var locations = list.Where(x => !App.systemCache.GlobalConfigsCache.MoveStockLocations.Contains(x.Local)).ToList();

                var pocoLocations = new MoveLocationEconnectorModel() {
                    MMS175 = locations,
                    PPS320 = fixLocations
                };

                var response = MoveLocationEndpointCall(pocoLocations);

                if (!string.IsNullOrEmpty(response.Item1)) {
                    message = "Erro ao movimentar artigo(s).";
                }

                if (pocoLocations.PPS320 != null) {
                    foreach (var pps320 in pocoLocations.PPS320) {
                        // deu erro??
                        var exist = response.Item2.Where(x => x.Item1.Trim().ToLower() == pps320.Item.Trim().ToLower() && x.Item2.Trim().ToLower() == pps320.Local.Trim().ToLower()).Count();

                        if (exist == 0) {
                            App.systemCache.MoveLocation.pocos.Remove(pps320);
                        }
                    }
                }

                if (pocoLocations.MMS175 != null) {
                    foreach (var mms175 in pocoLocations.MMS175) {
                        // deu erro??
                        var exist = response.Item2.Where(x => x.Item1.Trim().ToLower() == mms175.Item.Trim().ToLower() && x.Item2.Trim().ToLower() == mms175.Local.Trim().ToLower()).Count();

                        if (exist == 0) {
                            App.systemCache.MoveLocation.pocos.Remove(mms175);
                        }
                    }
                }

                return message;
            } catch (Exception ex) {
                logLogic.Insert(ex.Message, LogType.ERRO, MethodBase.GetCurrentMethod().DeclaringType.Name, App.systemCache.UserData.UserName);
                return ex.Message;
            }
        }

        private Tuple<string, List<Tuple<string, string>>> MoveLocationEndpointCall (MoveLocationEconnectorModel poco) {
            try {
                var webResponse = (HttpWebResponse)EconnetorInvoke.EndpoinConnector(EconnetorInvoke.EnderecosPedidos.MoveLocation, poco);
                var res = string.Empty;
                var encoding = Encoding.UTF8;

                using (var reader = new StreamReader(webResponse.GetResponseStream(), encoding)) {
                    res = reader.ReadToEnd();
                }

                if (webResponse.StatusCode == HttpStatusCode.OK) {
                    var json = JsonConvert.DeserializeObject<Tuple<string, List<Tuple<string, string>>>>(res);

                    var response = json.Item1.Replace("\"", "");
                    var message = "EndPoint Call -> MoveLocation:" + response;

                    logLogic.Insert(message, LogType.INFO, MethodBase.GetCurrentMethod().DeclaringType.Name, App.systemCache.UserData.UserName);
                    return json;
                } else {
                    logLogic.Insert(res, LogType.ERRO, MethodBase.GetCurrentMethod().DeclaringType.Name, App.systemCache.UserData.UserName);
                    return new Tuple<string, List<Tuple<string, string>>>(res, new List<Tuple<string, string>>());
                }
            } catch (Exception ex) {
                logLogic.Insert(ex.Message, LogType.ERRO, MethodBase.GetCurrentMethod().DeclaringType.Name, App.systemCache.UserData.UserName);
                return new Tuple<string, List<Tuple<string, string>>>(ex.Message, new List<Tuple<string, string>>());
            }
        }
    }
}
