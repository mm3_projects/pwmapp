﻿using PWM.Core;
using PWM.Models;
using PWM.Models.Expedition;
using PWM.Models.Requests;
using PWM.Models.Search;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace PWM.Logic {
    public class M3Logic {
        public DataTable GetAliasMITPOP (bool isRealEnvironment, string item) {
            var database = SystemConfigs.M3Environment(isRealEnvironment);
            return APIDeserializedDataLogic.GetData(new GeneralRequestParameters() {
                Query = Repository.GetItemMitpop(database, item)
            });
        }

        public DataTable GetAliasMITMAS (bool isRealEnvironment, string item) {
            var database = SystemConfigs.M3Environment(isRealEnvironment);
            return APIDeserializedDataLogic.GetData(new GeneralRequestParameters() {
                Query = Repository.GetItemMitmas(database, item)
            });
        }

        public DataTable GetWarehouses (bool isRealEnvironment, string facility) {
            var database = SystemConfigs.M3Environment(isRealEnvironment);
            return APIDeserializedDataLogic.GetData(new GeneralRequestParameters() {
                Query = Repository.GetWarehouses(database, facility)
            });
        }

        public List<ExpeditionModel> GetExpeditions (bool isRealEnvironment, string company, string divi, string faci, string warehouse, ExpeditionReadModel expedition, List<string> ignoreClientAlias) {
            var database = SystemConfigs.M3Environment(isRealEnvironment);
            var data = APIDeserializedDataLogic.GetData(new GeneralRequestParameters() {
                Query = Repository.GetExpedition(database, company, divi, faci, warehouse, expedition.Encomenda, expedition.Entrega, expedition.Sufixo, ignoreClientAlias)
            });

            return data.Rows
                .OfType<DataRow>()
                .Select(row => {
                    var poco = new ExpeditionModel() {
                        Alias = row["Alias"].ToString(),
                        AllocatedQty = int.Parse(row["AllocatedQty"].ToString()),
                        Container = row["Container"].ToString(),
                        Customer = row["Customer"].ToString(),
                        DeliveryNumber = long.Parse(row["DeliveryNumber"].ToString()),
                        Item = row["Item"].ToString(),
                        ItemDescription = row["ItemDescription"].ToString(),
                        LineSuffix = int.Parse(row["LineSuffix"].ToString()),
                        LocationOrder = row["LocationOrder"].ToString(),
                        LotNumber = row["LotNumber"].ToString(),
                        MaximumQty = int.Parse(row["MaximumQty"].ToString()),
                        MissingQty = int.Parse(row["MissingQty"].ToString()),
                        OrderLine = int.Parse(row["OrderLine"].ToString()),
                        OrderNumber = long.Parse(row["OrderNumber"].ToString()),
                        Packaging = row["Packaging"].ToString(),
                        PickingListSuffix = long.Parse(row["PickingListSuffix"].ToString()),
                        ReportingNumber = long.Parse(row["ReportingNumber"].ToString()),
                        StockTransactionType = decimal.Parse(row["StockTransactionType"].ToString()),
                        StockZone = row["StockZone"].ToString(),
                        TransportationFlow = row["TransportationFlow"].ToString()
                    };

                    return poco;
                })
                .ToList();
        }

        public List<ExpeditionPackedModel> GetPackedExpeditions (bool isRealEnvironment, string company, string warehouse, List<long> deliveries) {
            var database = SystemConfigs.M3Environment(isRealEnvironment);
            var data = APIDeserializedDataLogic.GetData(new GeneralRequestParameters() {
                Query = Repository.GetPackedExpeditions(database, company, warehouse, deliveries)
            });

            return data.Rows
                .OfType<DataRow>()
                .Select(row => {
                    var poco = new ExpeditionPackedModel() {
                        Box = row["Box"].ToString(),
                        DeliveryNumber = long.Parse(row["DeliveryNumber"].ToString()),
                        Item = row["Item"].ToString(),
                        Quantity = decimal.Parse(row["Quantity"].ToString())
                    };

                    return poco;
                })
                .ToList();
        }

        public List<SearchByProductModel> SearchByProduct (bool isRealEnvironment, string company, string faci, string item, string warehouse) {
            var database = SystemConfigs.M3Environment(isRealEnvironment);
            var data = APIDeserializedDataLogic.GetData(new GeneralRequestParameters() {
                Query = Repository.SearchByProduct(database, company, faci, item, warehouse)
            });

            return data.Rows
                   .OfType<DataRow>()
                   .Select(row => {
                       var poco = new SearchByProductModel() {
                           Estado = row["Estado"].ToString(),
                           Item = row["Item"].ToString(),
                           ItemDescription = row["ItemDescription"].ToString(),
                           Lote = row["Lote"].ToString(),
                           QtdAlc = decimal.Parse(row["QtdAlc"].ToString()),
                           QtdTotal = decimal.Parse(row["QtdTotal"].ToString()),
                           ReceiviedData = row["ReceiviedData"].ToString(),
                           Warehouse = row["Warehouse"].ToString(),
                           WarehouseLocal = row["WarehouseLocal"].ToString()
                       };

                       return poco;
                   })
                   .ToList();
        }

        public List<SearchByLocalModel> SearchByLocal (bool isRealEnvironment, string company, string faci, string local) {
            var database = SystemConfigs.M3Environment(isRealEnvironment);
            var data = APIDeserializedDataLogic.GetData(new GeneralRequestParameters() {
                Query = Repository.SearchByLocal(database, company, faci, local)
            });


            return data.Rows
                   .OfType<DataRow>()
                   .Select(row => {
                       var poco = new SearchByLocalModel() {
                           Estado = row["Estado"].ToString(),
                           Item = row["Item"].ToString(),
                           ItemDescription = row["ItemDescription"].ToString(),
                           QtdAlc = decimal.Parse(row["QtdAlc"].ToString()),
                           QtdTotal = decimal.Parse(row["QtdTotal"].ToString()),
                           Warehouse = row["Warehouse"].ToString(),
                           WarehouseLocal = row["WarehouseLocal"].ToString()
                       };

                       return poco;
                   })
                   .ToList();
        }

        public int CheckIfExistTipoCaixa (bool isRealEnvironment, string tipoCaixa) {
            var database = SystemConfigs.M3Environment(isRealEnvironment);
            var data = APIDeserializedDataLogic.GetData(new GeneralRequestParameters() {
                Query = Repository.GetBoxType(database, tipoCaixa)
            });

            return data.Rows.Count;
        }

        public DataTable GetStock (bool isRealEnvironment, string item, string warehouse, string local, string facility, string company) {
            var database = SystemConfigs.M3Environment(isRealEnvironment);
            return APIDeserializedDataLogic.GetData(new GeneralRequestParameters() {
                Query = Repository.GetStock(database, company, facility, item, warehouse, local)
            });
        }

        public int GetQtyBox (bool isRealEnvironment, string item) {
            var database = SystemConfigs.M3Environment(isRealEnvironment);
            var data = APIDeserializedDataLogic.GetData(new GeneralRequestParameters() {
                Query = Repository.GetBoxQuantity(database, item)
            });

            return int.Parse(data.Rows[0]["ECD1QT"].ToString());
        }
    }
}
