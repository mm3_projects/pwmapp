﻿using Acr.UserDialogs;
using PWM.Logic;
using PWM.Models;
using PWM.Models.Log;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PWM {
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MoveLocationRunning : ContentPage {
        #region dependencies
        M3Logic m3Logic = new M3Logic();
        LogLogic logLogic = new LogLogic();
        MoveLocationLogic moveLocationLogic = new MoveLocationLogic();
        #endregion

        #region properties
        public int rowSize = 40;
        public int columnSize = 100;
        public ObservableCollection<MoveLocationPoco> MoveLocationList { get; set; }
        ReadsValidationLogic readsValidationLogic = new ReadsValidationLogic();
        #endregion

        public MoveLocationRunning () {
            InitializeComponent();
            this.MoveLocationList = new ObservableCollection<MoveLocationPoco>();
            FillDataAndAdjustView();
        }

        private void FillDataAndAdjustView () {
            this.MoveLocationList = new ObservableCollection<MoveLocationPoco>();
            App.systemCache.MoveLocation.pocos.ForEach(x => this.MoveLocationList.Add(x));
            this.ListViewLocation.ItemsSource = this.MoveLocationList;
            var rowsCount = App.systemCache.MoveLocation.pocos.Count;
            var columnsCount = 5;

            this.ListViewLocation.HeightRequest = rowSize;
            this.ListViewLocation.WidthRequest = (columnsCount * columnSize);
            this.ListViewLocation.ItemsSource = App.systemCache.MoveLocation.pocos;
        }

        private void ButtonMovimentarTudo_Clicked (object sender, System.EventArgs e) {
            Movimentar(null);
        }

        private async void ButtonMovimentarLinha_Clicked (object sender, System.EventArgs e) {
            var selectedValue = this.ListViewLocation.SelectedItem;

            if (selectedValue == null) {
                await DisplayAlert("Localização", "Selecione uma linha", "OK");
                return;
            }

            var selected = (MoveLocationPoco)selectedValue;
            Movimentar(selected);
        }

        private async void ButtonEliminarLinha_Clicked (object sender, System.EventArgs e) {
            try {
                var selectedValue = this.ListViewLocation.SelectedItem;

                if (selectedValue == null) {
                    await DisplayAlert("Localização", "Selecione uma linha", "OK");
                    return;
                }

                var selected = (MoveLocationPoco)selectedValue;

                bool answer = await DisplayAlert("Localização", "Tem certeza que quer eliminar a linha selecionada?", "Sim", "Não");
                if (answer) {
                    App.systemCache.MoveLocation.pocos.Remove(selected);
                    FillDataAndAdjustView();
                } else {
                    this.ListViewLocation.SelectedItem = null;
                }
            } catch (Exception ex) {
                await DisplayAlert("Localização", "Ocorreu um erro ao tentar remover a linha. Ex:" + ex.Message, "OK");
            }
        }

        private void ButtonEliminarTudo_Clicked (object sender, System.EventArgs e) {
            try {
                App.systemCache.MoveLocation.pocos = new List<MoveLocationPoco>();
                FillDataAndAdjustView();
            } catch (Exception ex) {
                DisplayAlert("Localização", "Ocorreu um erro ao tentar eliminar tudo. Ex:" + ex.Message, "OK");
            }
        }

        private void TapGestureBackButton_Tapped (object sender, EventArgs e) {
            App.Current.MainPage = new MoveLocationMenu();
        }

        private string ValidateInput (string toLocation) {
            try {
                #region ValidateLocal
                if (!string.IsNullOrEmpty(toLocation) && toLocation.Length > 0) {
                    var result = readsValidationLogic.ValidateLocalRead(toLocation.Trim());

                    if (!string.IsNullOrEmpty(result.Error)) {
                        return result.Error;
                    }

                    App.systemCache.MoveLocation.ToWarehouse = result.Warehouse;
                    App.systemCache.MoveLocation.ToLocal = result.Local;
                }
                #endregion

                return string.Empty;
            } catch (Exception ex) {
                logLogic.Insert(ex.Message, LogType.ERRO, MethodBase.GetCurrentMethod().DeclaringType.Name, App.systemCache.UserData.UserName);
                return "Local inválido";
            }
        }

        private async void Movimentar (MoveLocationPoco movePoco) {
            PromptResult result = await UserDialogs.Instance.PromptAsync(new PromptConfig() {
                CancelText = "Cancelar",
                InputType = InputType.Default,
                Message = "Introduza a localização destino",
                Title = "Movimentar",
            });

            if (!string.IsNullOrEmpty(result.Text)) {
                using (UserDialogs.Instance.Loading("Loading...", null, null, true)) {
                    var response = await Task.Run(() => ValidateInput(result.Text));

                    if (!string.IsNullOrEmpty(response)) {
                        await DisplayAlert("Localização", response, "OK");
                        return;
                    }

                    this.ButtonEliminarLinha.IsEnabled = false;
                    this.ButtonEliminarTudo.IsEnabled = false;
                    this.ButtonMovimentarLinha.IsEnabled = false;
                    this.ButtonMovimentarTudo.IsEnabled = false;

                    var list = new List<MoveLocationPoco>();
                    if (movePoco == null) {
                        list = App.systemCache.MoveLocation.pocos.Select(x => {
                            var poco = x;
                            poco.ToLocation = App.systemCache.MoveLocation.ToLocal;
                            return poco;
                        }).ToList();
                    } else {
                        movePoco.ToLocation = App.systemCache.MoveLocation.ToLocal;
                        list.Add(movePoco);
                    }

                    var responseProcess = await Task.Run(() => moveLocationLogic.Process(list));

                    if (!string.IsNullOrEmpty(responseProcess)) {
                        this.ButtonEliminarLinha.IsEnabled = true;
                        this.ButtonEliminarTudo.IsEnabled = true;
                        this.ButtonMovimentarLinha.IsEnabled = true;
                        this.ButtonMovimentarTudo.IsEnabled = true;
                        FillDataAndAdjustView();
                        await DisplayAlert("Localização", responseProcess, "OK");
                        return;
                    } else {
                        this.ButtonEliminarLinha.IsEnabled = true;
                        this.ButtonEliminarTudo.IsEnabled = true;
                        this.ButtonMovimentarLinha.IsEnabled = true;
                        this.ButtonMovimentarTudo.IsEnabled = true;
                        FillDataAndAdjustView();
                        await DisplayAlert("Localização", "Movimentação feita com sucesso", "OK");
                        return;
                    }
                }
            }
        }
    }
}