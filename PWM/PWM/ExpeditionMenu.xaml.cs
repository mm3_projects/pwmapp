﻿using PWM.Core;
using PWM.Logic;
using PWM.Models.Log;
using System;
using System.Linq;
using System.Reflection;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PWM {
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ExpeditionMenu : MasterDetailPage {
        #region dependencies
        ExpeditionBlockedLogic expeditionBlockedLogic = new ExpeditionBlockedLogic();
        LogLogic logLogic = new LogLogic();
        #endregion

        #region properties
        public Expedition expeditionPage { get; set; }
        #endregion

        public ExpeditionMenu () {
            InitializeComponent();

            this.expeditionPage = new Expedition();
            Detail = new NavigationPage(this.expeditionPage);
            IsPresented = false;
        }

        private void GoLimparDados (object sender, System.EventArgs e) {
            IsPresented = false;
            App.systemCache.ExpeditionClearData();
            Application.Current.MainPage = new ExpeditionMenu();
        }

        private async void GoNovaLeitura (object sender, System.EventArgs e) {
            var mensagem = "Tem a certeza que quer limpar as leituras?";

            if (App.systemCache.ExpeditionData.FailedPacking.Any()) {
                mensagem = "Falta enviar dados para o M3. Irá perder informação. Tem a certeza que quer limpar as leituras?";
            }

            bool answer = await DisplayAlert("Expedição", mensagem, "Sim", "Não");
            if (answer) {
                try {
                    IsPresented = false;
                    expeditionBlockedLogic.Delete(App.systemCache.ExpeditionData.ExpeditionOrdersPicked.Select(x => x.All).ToList(), App.systemCache.UserData.UserID, App.systemCache.DeviceData.DeviceID);

                    App.systemCache.ExpeditionClearAllData();
                    Application.Current.MainPage = new ExpeditionRead();
                } catch (Exception ex) {
                    logLogic.Insert(ex.Message, LogType.ERRO, MethodBase.GetCurrentMethod().DeclaringType.Name, App.systemCache.UserData.UserName);
                    await DisplayAlert("Expedição", "Ocorreu um erro ao terminar expedição", "Sim", "Não");
                    return;
                }
            }
        }

        private async void GoToMenu (object sender, System.EventArgs e) {
            var mensagem = "Tem a certeza que quer voltar ao menu?";

            if (App.systemCache.ExpeditionData.FailedPacking.Any()) {
                mensagem = "Falta enviar dados para o M3. Irá perder informação. Tem a certeza que quer voltar ao menu?";
            }

            bool answer = await DisplayAlert("Expedição", mensagem, "Sim", "Não");
            if (answer) {
                try {
                    expeditionBlockedLogic.Delete(App.systemCache.ExpeditionData.ExpeditionOrdersPicked.Select(x => x.All).ToList(), App.systemCache.UserData.UserID, App.systemCache.DeviceData.DeviceID);
                    App.systemCache.ExpeditionClearAllData();

                    IsPresented = false;
                    Application.Current.MainPage = new MenuMenu();
                } catch (Exception ex) {
                    logLogic.Insert(ex.Message, LogType.ERRO, MethodBase.GetCurrentMethod().DeclaringType.Name, App.systemCache.UserData.UserName);
                    await DisplayAlert("Expedição", "Ocorreu um erro ao terminar expedição", "Sim", "Não");
                    return;
                }
            }
        }

        private void GoEmbalados (object sender, System.EventArgs e) {
            IsPresented = false;
            Application.Current.MainPage = new ExpeditionPacked();
        }

        private void GoPesquisarLocal (object sender, System.EventArgs e) {
            IsPresented = false;
            Application.Current.MainPage = new SearchByLocal(GlobalFunctions.GetMenuNamespace(this.GetType().Namespace, this.GetType().Name));
        }

        private void GoPesquisarProduto (object sender, System.EventArgs e) {
            IsPresented = false;
            Application.Current.MainPage = new SearchByProduct(GlobalFunctions.GetMenuNamespace(this.GetType().Namespace, this.GetType().Name));
        }
    }
}