﻿using System;

namespace PWM.Models.Mittra {
    public class MittraPoco {
        public decimal MTCONO { get; set; } //Company
        public string MTWHLO { get; set; } //Armazém
        public string MTITNO { get; set; } //Artigo
        public DateTime CREATEDATE { get; set; }
        public DateTime TRANSACTIONDATE { get; set; }
        public string MTRESP { get; set; } //Responsável
        public string MTTTID { get; set; } //TransactionIdentity
        public decimal MTTTYP { get; set; } //StockTransactionType
        public string MTSLTP { get; set; } //StockZone
        public string MTWHSL { get; set; } //Location
        public string MTBANO { get; set; } //LotNumber
        public string MTRIDN { get; set; } //OrderNumber
        public decimal? MTRIDO { get; set; } //OrderOperation
        public decimal MTRIDL { get; set; } //OrderLine
        public decimal MTRIDI { get; set; } //DeliveryNumber
        public decimal MTPLSX { get; set; } //Suffix
        public decimal MTTRQT { get; set; } //Quantity
    }
}
