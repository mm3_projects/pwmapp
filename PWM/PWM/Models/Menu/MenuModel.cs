﻿using System;

namespace PWM.Models.Menu
{
    public class MenuModel
    {
        public string Image1 { get; set; }

        public string Title1 { get; set; }

        public string NavigateType1 { get; set; }

        public string Image2 { get; set; }

        public string Title2 { get; set; }

        public string NavigateType2 { get; set; }

        public string Image3 { get; set; }

        public string Title3 { get; set; }

        public string NavigateType3 { get; set; }
    }
}
