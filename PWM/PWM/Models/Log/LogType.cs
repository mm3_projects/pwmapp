﻿namespace PWM.Models.Log
{
    public enum LogType
    {
        AVISO,
        ERRO,
        INFO
    }
}
