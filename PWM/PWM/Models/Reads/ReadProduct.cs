﻿namespace PWM.Models.Reads {
    public class ReadProduct {
        public string lblItemID { get; set; }

        public string lblLotID { get; set; }

        public string lblOrderIDBox { get; set; }

        public string lblQuantity { get; set; }
    }
}
