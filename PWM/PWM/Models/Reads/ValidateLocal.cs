﻿namespace PWM.Models.Reads {
    public class ValidateLocal {
        public string Warehouse { get; set; }

        public string Local { get; set; }

        public string Error { get; set; }
    }
}
