﻿namespace PWM.Models.Reads {
    public class ValidateItemLot {
        public string Item { get; set; }

        public string Lot { get; set; }

        public string Description { get; set; }

        public int CheckLot { get; set; }

        public string Error { get; set; }
    }
}
