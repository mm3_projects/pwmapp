﻿using System.Collections.Generic;

namespace PWM.Models.PWMConfigs {
    public class GlobalConfigsCache {
        public List<string> MoveStockLocations { get; set; }

        public string Responsible { get; set; }

        public List<string> IgnoreAlias { get; set; }

        public GlobalConfigsCache () {
            MoveStockLocations = new List<string>();
            Responsible = string.Empty;
            IgnoreAlias = new List<string>();
        }
    }
}
