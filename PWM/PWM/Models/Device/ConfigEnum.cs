﻿namespace PWM.Models.Device {
    public static class ConfigEnum {
        public static string WarehouseConfig = "Warehouse";
        public static string FacilityConfig = "Facility";
        public static string DivisonConfig = "Division";
        public static string CompanyConfig = "Company";
        public static string RealEnvironmentConfig = "RealEnvironment";
        public static string PrinterNameConfig = "PrinterName";
        public static string FileNameToPrintConfig = "FilenameToPrint";
        public static string ExpeditionToLocationConfig = "ExpeditionToLocation";
        public static string LotValidationConfig = "LotValidation";
        public static string QMSLocationsConfig = "QMSLocations";
        public static string ResponsibleConfig = "Responsible";
        public static string MoveStockLocationsConfig = "MoveStockLocations";
        public static string IgnoreClientAliasConfig = "IgnoreClientAlias";
    }
}
