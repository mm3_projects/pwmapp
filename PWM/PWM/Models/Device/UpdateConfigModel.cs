﻿namespace PWM.Models.Device {
    public class UpdateConfigModel {
        public long ConfigID { get; set; }

        public long DeviceID { get; set; }

        public string ConfigValue { get; set; }
    }
}
