﻿using System.Collections.Generic;

namespace PWM.Models.Device {
    public class DeviceModelCache {
        public long DeviceID { get; set; }

        public string DeviceSerial { get; set; }

        public Dictionary<string, long> ConfigsID { get; set; }

        public string Warehouse { get; set; }

        public string Company { get; set; }

        public string Division { get; set; }

        public string Facility { get; set; }

        public bool RealEnvironment { get; set; }

        public string PrinterName { get; set; }

        public string FileNameToPrint { get; set; }

        public string ExpeditionToLocation { get; set; }

        public bool LotValidation { get; set; }

        public DeviceModelCache () {
            DeviceID = 0;
            DeviceSerial = string.Empty;
            ConfigsID = new Dictionary<string, long>();
            Warehouse = string.Empty;
            Company = string.Empty;
            Division = string.Empty;
            Facility = string.Empty;
            RealEnvironment = false;
            PrinterName = string.Empty;
            FileNameToPrint = string.Empty;
            ExpeditionToLocation = string.Empty;
            LotValidation = false;
        }
    }
}
