﻿namespace PWM.Models.Requests {
    public class GeneralRequestParameters {
        public string Query {
            get;
            set;
        }
    }
}
