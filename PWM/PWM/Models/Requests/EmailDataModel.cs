﻿using System.IO;

namespace PWM.Models.Requests {
    public class EmailDataModel {
        public string Assunto { get; set; }
        public string[] Para { get; set; }
        public string[] CC { get; set; }
        public string Body { get; set; }
        public Stream AnexoStream { get; set; }
        public string NomeFicheiroAnexo { get; set; }
        public string Anexo { get; set; }
    }
}
