﻿using System.Collections.Generic;

namespace PWM.Models.Expedition
{
    public class ExpeditionValidateData
    {
        public string ErroLocal { get; set; }

        public string ErroProdutoLote { get; set; }

        public string ErroEntrega { get; set; }

        public string Erro { get; set; }

        public List<ExpeditionModel> DataToShow { get; set; }

        public List<ExpeditionModel> ListByLocal { get; set; }
    }
}
