﻿namespace PWM.Models.Expedition {
    public class NWS422
    {
        public string Company { get; set; }

        public string Divi { get; set; }

        public long ReportingNumber { get; set; }

        public string Location { get; set; }

        public int Quantity { get; set; }
    }
}
