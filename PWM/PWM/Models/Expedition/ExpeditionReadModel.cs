﻿namespace PWM.Models.Expedition {
    public class ExpeditionReadModel {
        public long Encomenda { get; set; }

        public long Entrega { get; set; }

        public int Sufixo { get; set; }

        public string All { get; set; }
    }
}
