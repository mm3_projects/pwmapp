﻿namespace PWM.Models.Expedition
{
    public class PackingModel
    {
        public MWS420 AddLine { get; set; }

        public NWS422 Pack { get; set; }
    }
}
