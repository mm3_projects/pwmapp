﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace PWM.Models.Expedition {
    public class ExpeditionModelCache {
        public string ExpeditionLocalLbl { get; set; }

        public string ExpeditionLocal { get; set; }

        public string ExpeditionProduct { get; set; }

        public string ExpeditionLot { get; set; }

        public string ExpeditionDeliveryLbl { get; set; }

        public long ExpeditionDelivery { get; set; }

        public string ExpeditionBox { get; set; }

        public ObservableCollection<ExpeditionReadModel> ExpeditionOrdersPicked { get; set; }

        public ObservableCollection<ExpeditionModel> ExpeditionInfo { get; set; }

        public ObservableCollection<ExpeditionModel> ExpeditionInfoFiltered { get; set; }

        public List<PackingModel> FailedPacking { get; set; }

        public Dictionary<long, List<string>> DeliveryBoxDic { get; set; }

        public Dictionary<long, string> DeliveryTipoCaixa { get; set; }

        public ExpeditionModelCache()
        {
            ExpeditionLocalLbl = string.Empty;
            ExpeditionLocal = string.Empty;
            ExpeditionProduct = string.Empty;
            ExpeditionLot = string.Empty;
            ExpeditionDeliveryLbl = string.Empty;
            ExpeditionDelivery = 0;
            ExpeditionBox = string.Empty;
            ExpeditionOrdersPicked = new ObservableCollection<ExpeditionReadModel>();
            ExpeditionInfo = new ObservableCollection<ExpeditionModel>();
            ExpeditionInfoFiltered = new ObservableCollection<ExpeditionModel>();
            FailedPacking = new List<PackingModel>();
            DeliveryBoxDic = new Dictionary<long, List<string>>();
            DeliveryTipoCaixa = new Dictionary<long, string>();
        }
    }
}
