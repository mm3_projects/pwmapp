﻿namespace PWM.Models {
    public class ExpeditionModel {
        public long OrderNumber { get; set; }

        public int OrderLine { get; set; }

        public int LineSuffix { get; set; }

        public long DeliveryNumber { get; set; }

        public long PickingListSuffix { get; set; }

        public string Item { get; set; }

        public string ItemDescription { get; set; }

        public string LocationOrder { get; set; }

        public int AllocatedQty { get; set; }

        public int MissingQty { get; set; }

        public string Packaging { get; set; }

        public int MaximumQty { get; set; }

        public string Customer { get; set; }

        public string Alias { get; set; }

        public string LotNumber { get; set; }

        public string Container { get; set; }

        public string TransportationFlow { get; set; }

        public long ReportingNumber { get; set; }

        public string StockZone { get; set; }

        public decimal StockTransactionType { get; set; }
    }
}
