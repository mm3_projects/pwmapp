﻿namespace PWM.Models.Expedition {
    public class MWS420 {
        public string Company { get; set; }

        public string Divi { get; set; }

        public long DeliveryNumber { get; set; }

        public long PickingListSuffix { get; set; }

        public string Warehouse { get; set; }

        public string Location { get; set; }

        public string LotNumber { get; set; }

        public string Container { get; set; }

        public long OrderNumber { get; set; }

        public int OrderLine { get; set; }

        public int LineSuffix { get; set; }

        public string ItemNumber { get; set; }

        public int StockTransactionType { get; set; }

        public int? OrderOperation { get; set; }

        public int? YesNo { get; set; }

        public decimal PackedQuantity { get; set; }

        public string PackageNumber { get; set; }

        public string Packaging { get; set; }
    }
}
