﻿namespace PWM.Models {
    public class ExpeditionPackedModel {
        public string Item { get; set; }

        public long DeliveryNumber { get; set; }

        public string Box { get; set; }

        public decimal Quantity { get; set; }
    }
}
