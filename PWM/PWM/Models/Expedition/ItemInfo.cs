﻿namespace PWM.Models
{
    public class ItemInfo
    {
        public string Item { get; set; }
        public int Lot { get; set; }
        public string Description { get; set; }
    }
}
