﻿namespace PWM.Models.MoveLocation {
    public class MoveLocationStock {
        public int Quantity { get; set; }

        public string LotNumber { get; set; }

        public string ReceivingNumber { get; set; }
    }
}
