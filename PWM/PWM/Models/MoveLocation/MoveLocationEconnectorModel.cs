﻿using System.Collections.Generic;

namespace PWM.Models.MoveLocation {
    public class MoveLocationEconnectorModel
    {
        public List<MoveLocationPoco> MMS175 { get; set; }

        public List<MoveLocationPoco> PPS320 { get; set; }
    }
}
