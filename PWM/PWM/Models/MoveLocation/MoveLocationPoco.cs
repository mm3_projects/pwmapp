﻿namespace PWM.Models {
    public class MoveLocationPoco {
        public string Company { get; set; }

        public string Division { get; set; }

        public string Item { get; set; }

        public string ItemDescription { get; set; }

        public string Lot { get; set; }

        public string Local { get; set; }

        public string Warehouse { get; set; }

        public string ToLocation { get; set; }

        public int MoveUN { get; set; }

        public string ReceivingNumber { get; set; }

        public string Responsible { get; set; }
    }
}
