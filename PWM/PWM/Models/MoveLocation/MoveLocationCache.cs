﻿using System;
using System.Collections.Generic;

namespace PWM.Models.MoveLocation {
    public class MoveLocationCache {
        public string Itemlbl { get; set; }

        public string ItemDescriptionlbl { get; set; }

        public string Lotlbl { get; set; }

        public string Lot { get; set; }

        public string Local { get; set; }

        public string Warehouse { get; set; }

        public string ToLocal { get; set; }

        public string ToWarehouse { get; set; }

        public int StockQuantity { get; set; }

        public int ItemStandardQuantity { get; set; }

        public string ReceivingNumber { get; set; }

        public string QMS { get; set; }

        public List<MoveLocationPoco> pocos { get; set; }

        public MoveLocationCache()
        {
            Itemlbl = string.Empty;
            ItemDescriptionlbl = string.Empty;
            Lotlbl = string.Empty;
            Lot = string.Empty;
            Local = string.Empty;
            Warehouse = string.Empty;
            pocos = new List<MoveLocationPoco>();
            StockQuantity = 0;
            ItemStandardQuantity = 0;
            ReceivingNumber = string.Empty;
            QMS = string.Empty;
        }
    }
}
