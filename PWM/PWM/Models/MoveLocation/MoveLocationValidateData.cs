﻿using System.Collections.Generic;

namespace PWM.Models.MoveLocation
{
    public class MoveLocationValidateData
    {
        public string ErrorLocal { get; set; }

        public string ErrorItemLot { get; set; }

        public string Error { get; set; }

        public string Item { get; set; }

        public string ItemDescription { get; set; }

        public string Lot { get; set; }
    }
}
