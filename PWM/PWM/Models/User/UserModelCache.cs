﻿using System.Data;

namespace PWM.Models.User {
    public class UserModelCache {
        public long UserID { get; set; }

        public DataTable Permissions { get; set; }

        public string UserName { get; set; }

        public UserModelCache () {
            UserID = 0;
            Permissions = new DataTable();
            UserName = string.Empty;
        }
    }
}
