﻿using System;

namespace PWM.Models.Search {
    public class SearchByProductModel {
        public string Warehouse { get; set; }

        public string Item { get; set; }

        public string Lote { get; set; }

        public string ReceiviedData { get; set; }

        public string ItemDescription { get; set; }

        public string WarehouseLocal { get; set; }

        public decimal QtdTotal { get; set; }

        public decimal QtdAlc { get; set; }

        public string Estado { get; set; }
    }
}
