﻿namespace PWM.Models.Search {
    public class SearchByLocalModel {

        public string Item { get; set; }

        public decimal QtdTotal { get; set; }

        public decimal QtdAlc { get; set; }

        public string Estado { get; set; }

        public string Warehouse { get; set; }

        public string WarehouseLocal { get; set; }

        public string ItemDescription { get; set; }
    }
}
