﻿using PWM.Logic;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PWM {
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MenuMenu : MasterDetailPage {
        #region dependencies
        UserLogic userLogic = new UserLogic();
        LogLogic logLogic = new LogLogic();
        #endregion

        public MenuMenu () {
            InitializeComponent();
            Detail = new NavigationPage(new Menu());
        }

        private async void GoChangeWarehouse (object sender, System.EventArgs e) {
            IsPresented = false;
            await Detail.Navigation.PushAsync(new Warehouse());
        }

        private void GoLogout (object sender, System.EventArgs e) {
            IsPresented = false;
            Application.Current.MainPage = new Login();
        }

        private async void GoAjuda (object sender, System.EventArgs e) {
            IsPresented = false;
            await Detail.Navigation.PushAsync(new Help());
        }
    }
}