﻿using Acr.UserDialogs;
using PWM.Logic;
using PWM.Models;
using PWM.Models.Log;
using PWM.Models.MoveLocation;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PWM {
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MoveLocation : ContentPage {
        #region dependencies
        LogLogic logLogic = new LogLogic();
        M3Logic m3Logic = new M3Logic();
        ReadsValidationLogic readsValidationLogic = new ReadsValidationLogic();
        #endregion

        #region properties
        public int checkLot = 0;
        public int TextBoxLength { get; set; }
        #endregion

        public MoveLocation () {
            InitializeComponent();

            this.TextBoxData.IsEnabled = true;
            this.txtCaixaCompleta.Text = string.Empty;
            this.lblUnPorCaixa.Text = string.Empty;
            this.txtUnAdicionais.Text = string.Empty;
            this.lblMovimentar.Text = string.Empty;
            this.txtUnAdicionais.IsEnabled = false;
            this.txtCaixaCompleta.IsEnabled = false;
            this.ButtonUnidades.IsEnabled = false;
            FillLocationsMoveStock();
        }

        private void FillLocationsMoveStock () {
            try {
                var list = new List<string>();
                list.Add("Sem Loc. Fixa");
                list.AddRange(App.systemCache.GlobalConfigsCache.MoveStockLocations);
                locationsDropDown.ItemsSource = list;
                locationsDropDown.SelectedIndex = 0;
                locationsDropDown.IsEnabled = false;
            } catch (Exception ex) {
                DisplayAlert("Localização", ex.Message, "OK");
                logLogic.Insert("Erro FillLocationsMoveStock:" + ex.Message, LogType.ERRO, MethodBase.GetCurrentMethod().DeclaringType.Name, App.systemCache.UserData.UserName);
            }
        }

        private async void ContentPage_Appearing (object sender, EventArgs e) {
            await Task.Delay(600);
            this.TextBoxData.Focus();
        }

        private void TextBoxData_TextChanged (object sender, TextChangedEventArgs e) {
            if (this.TextBoxData.Text.Trim().Length > 0 &&
                this.TextBoxData.Text.Trim().Length - this.TextBoxLength != 1 &&
                this.TextBoxData.Text.Trim().Length > this.TextBoxLength) {
                ValidarInput();
                this.TextBoxData.Focus();
            } else {
                this.TextBoxLength = this.TextBoxData.Text.Trim().Length;

                if (this.TextBoxLength > 0) {
                    this.ButtonOk.IsEnabled = true;
                } else {
                    this.ButtonOk.IsEnabled = false;
                }
            }
        }

        private string UpdateValues () {
            try {
                var valueUnCaixaCompleta = App.systemCache.MoveLocation.ItemStandardQuantity;

                if (valueUnCaixaCompleta != 0) {
                    this.lblUnPorCaixa.Text = valueUnCaixaCompleta.ToString();
                }

                var valorCaixaCompleta = string.IsNullOrEmpty(this.txtCaixaCompleta.Text) ? "0" : this.txtCaixaCompleta.Text;
                var unAdicionais = string.IsNullOrEmpty(this.txtUnAdicionais.Text) ? "0" : this.txtUnAdicionais.Text;
                var totalValue = (valueUnCaixaCompleta * int.Parse(valorCaixaCompleta)) + int.Parse(unAdicionais);
                this.lblMovimentar.Text = totalValue.ToString();

                return string.Empty;
            } catch (Exception ex) {
                return ex.Message;
            }
        }

        private void ButtonUnidades_Clicked (object sender, EventArgs e) {
            this.txtCaixaCompleta.Text = string.Empty;
            this.lblUnPorCaixa.Text = string.Empty;
            this.txtUnAdicionais.Text = string.Empty;
            this.lblMovimentar.Text = string.Empty;
            this.TextBoxData.IsEnabled = false;
        }

        private void ButtonConfirm_Clicked (object sender, EventArgs e) {
            try {
                if (string.IsNullOrEmpty(this.lblMovimentar.Text)) {
                    DisplayAlert("Localização", "Não existe quantidade a movimentar", "OK");
                    return;
                }

                var qtdMovimentar = int.Parse(this.lblMovimentar.Text);

                if (qtdMovimentar == 0) {
                    DisplayAlert("Localização", "A quantidade a movimentar deve ser superior a 0", "OK");
                    return;
                }

                if (App.systemCache.MoveLocation.StockQuantity < qtdMovimentar) {
                    DisplayAlert("Localização", "Não existe quantidade necessária em stock", "OK");
                    return;
                }

                var additionalUn = string.IsNullOrEmpty(this.txtUnAdicionais.Text) ? "0" : this.txtUnAdicionais.Text;
                var caixaCompleta = string.IsNullOrEmpty(this.txtCaixaCompleta.Text) ? "0" : this.txtCaixaCompleta.Text;
                var moveUn = string.IsNullOrEmpty(this.lblMovimentar.Text) ? "0" : this.lblMovimentar.Text;
                var unByBox = string.IsNullOrEmpty(this.lblUnPorCaixa.Text) ? "0" : this.lblUnPorCaixa.Text;

                App.systemCache.MoveLocation.pocos.Add(new MoveLocationPoco() {
                    Item = this.lblProduto.Text.Trim(),
                    Local = App.systemCache.MoveLocation.Local,
                    Lot = this.lblLote.Text == null ? string.Empty : this.lblLote.Text.Trim(),
                    MoveUN = int.Parse(moveUn),
                    Warehouse = App.systemCache.MoveLocation.Warehouse,
                    Company = App.systemCache.DeviceData.Company,
                    Division = App.systemCache.DeviceData.Division,
                    ReceivingNumber = App.systemCache.MoveLocation.ReceivingNumber,
                    Responsible = App.systemCache.GlobalConfigsCache.Responsible,
                    ItemDescription = this.lblProdutoDesc.Text.Trim()
                });

                this.txtUnAdicionais.Text = string.Empty;
                this.txtCaixaCompleta.Text = string.Empty;
                this.lblProduto.Text = string.Empty;
                this.lblProdutoDesc.Text = string.Empty;
                this.lblLocal.Text = string.Empty;
                this.lblLote.Text = string.Empty;
                this.lblMovimentar.Text = string.Empty;
                this.lblUnPorCaixa.Text = string.Empty;

                this.TextBoxData.IsEnabled = true;
                this.TextBoxData.Focus();
                this.TextBoxData.Placeholder = "Introduza o local";
                this.txtCaixaCompleta.IsEnabled = false;
                this.ButtonUnidades.IsEnabled = false;
                this.txtUnAdicionais.IsEnabled = false;
                locationsDropDown.IsEnabled = false;
                locationsDropDown.SelectedIndex = 0;
            } catch (Exception ex) {
                DisplayAlert("Localização", ex.Message, "OK");
                logLogic.Insert("Erro Confirm:" + ex.Message, LogType.ERRO, MethodBase.GetCurrentMethod().DeclaringType.Name, App.systemCache.UserData.UserName);
            }
        }

        private void TextBoxData_Unfocused (object sender, FocusEventArgs e) {
            if (this.TextBoxData.IsEnabled) {
                this.TextBoxData.Focus();
            }
        }

        private void ButtonOk_Clicked (object sender, EventArgs e) {
            ValidarInput();
        }

        private async void ValidarInput () {
            using (UserDialogs.Instance.Loading("Loading...", null, null, true)) {
                try {
                    this.ButtonOk.IsEnabled = false;
                    var response = await Task.Run(() => ValidateInput());

                    if (!string.IsNullOrEmpty(response)) {
                        this.TextBoxData.Text = string.Empty;
                        this.ButtonOk.IsEnabled = true;
                        await DisplayAlert("Localização", response, "OK");
                        return;
                    }

                    #region Item
                    if (string.IsNullOrEmpty(lblProduto.Text) || (string.IsNullOrEmpty(lblLote.Text) && checkLot != 0)) {
                        this.lblProduto.Text = App.systemCache.MoveLocation.Itemlbl;
                        this.lblProdutoDesc.Text = App.systemCache.MoveLocation.ItemDescriptionlbl;

                        if (string.IsNullOrEmpty(this.lblLote.Text) && checkLot == 0 || (!string.IsNullOrEmpty(App.systemCache.MoveLocation.Lotlbl) && checkLot != 0)) {
                            this.lblLote.Text = App.systemCache.MoveLocation.Lotlbl;
                            this.TextBoxData.Placeholder = "Introduza o local";
                            this.ButtonOk.IsEnabled = true;
                            this.TextBoxData.Text = string.Empty;
                            locationsDropDown.IsEnabled = true;
                            return;
                        } else {
                            this.TextBoxData.Placeholder = "Introduza o lote";
                            this.ButtonOk.IsEnabled = true;
                            this.TextBoxData.Text = string.Empty;
                            return;
                        }
                    }
                    #endregion

                    #region Local
                    if (string.IsNullOrEmpty(this.lblLocal.Text)) {
                        var result = FillStockQuantity();

                        if (!string.IsNullOrEmpty(result)) {
                            await DisplayAlert("Localização", result, "OK");
                            this.TextBoxData.Text = string.Empty;
                            return;
                        }

                        this.lblLocal.Text = this.TextBoxData.Text.Trim();
                        this.TextBoxData.Placeholder = string.Empty;
                        this.ButtonOk.IsEnabled = false;
                        this.TextBoxData.Text = string.Empty;
                        this.TextBoxData.IsEnabled = false;
                        this.txtCaixaCompleta.IsEnabled = true;
                        this.txtUnAdicionais.IsEnabled = true;
                        this.ButtonUnidades.IsEnabled = true;
                        return;
                    }
                    #endregion
                } catch (Exception ex) {
                    await DisplayAlert("Localização", ex.Message, "OK");
                    logLogic.Insert("Erro ValidarInput:" + ex.Message, LogType.ERRO, MethodBase.GetCurrentMethod().DeclaringType.Name, App.systemCache.UserData.UserName);
                }
            }
        }

        private string FillStockQuantity () {
            try {
                var stockList = m3Logic.GetStock(
                        App.systemCache.DeviceData.RealEnvironment,
                        this.lblProduto.Text.Trim(),
                        App.systemCache.MoveLocation.Warehouse,
                        App.systemCache.MoveLocation.Local,
                        App.systemCache.DeviceData.Facility,
                        App.systemCache.DeviceData.Company);

                if (App.systemCache.DeviceData.LotValidation) {
                    if (stockList.Rows.Count == 0) {
                        return "Não existe stock nesta localização para este artigo";
                    }

                    var lotExist = stockList.Rows.OfType<DataRow>().Where(x => x["LotNumber"].ToString().Trim() == App.systemCache.MoveLocation.Lotlbl.Trim()).FirstOrDefault();

                    if (lotExist == null) {
                        return "Não existe stock com os parametros definidos";
                    }

                    App.systemCache.MoveLocation.Lot = App.systemCache.MoveLocation.Lotlbl.Trim();
                    App.systemCache.MoveLocation.StockQuantity = int.Parse(lotExist["Quantity"].ToString());
                    App.systemCache.MoveLocation.ReceivingNumber = lotExist["ReceivingNumber"].ToString();
                } else {
                    var firstLot = stockList.Rows[0];

                    if (firstLot != null) {
                        App.systemCache.MoveLocation.Lot = firstLot["LotNumber"].ToString();
                        App.systemCache.MoveLocation.StockQuantity = int.Parse(firstLot["Quantity"].ToString());
                        App.systemCache.MoveLocation.ReceivingNumber = firstLot["ReceivingNumber"].ToString();
                    } else {
                        return "Não existe stock com os parametros definidos";
                    }
                }

                return string.Empty;
            } catch (Exception ex) {
                DisplayAlert("Localização", ex.Message, "OK");
                logLogic.Insert("Erro FillStockQuantity:" + ex.Message, LogType.ERRO, MethodBase.GetCurrentMethod().DeclaringType.Name, App.systemCache.UserData.UserName);
                return ex.Message;
            }
        }

        private string ValidateInput () {
            var model = new MoveLocationValidateData();

            try {
                #region ValidateItem
                //Leitura codigo EAN13 ou ITF14 ou Code39
                if (string.IsNullOrEmpty(this.lblProduto.Text) && this.TextBoxData.Text.Length > 0) {
                    var result = readsValidationLogic.ValidateProdutoLote(this.TextBoxData.Text.Trim(), checkLot);

                    if (!string.IsNullOrEmpty(result.Error)) {
                        return result.Error;
                    }

                    checkLot = result.CheckLot;
                    App.systemCache.MoveLocation.Itemlbl = result.Item;
                    App.systemCache.MoveLocation.ItemDescriptionlbl = result.Description;
                    App.systemCache.MoveLocation.Lotlbl = result.Lot;

                    var itemStandartQty = m3Logic.GetQtyBox(App.systemCache.DeviceData.RealEnvironment, App.systemCache.MoveLocation.Itemlbl);
                    App.systemCache.MoveLocation.ItemStandardQuantity = itemStandartQty;

                    return string.Empty;
                } else if (string.IsNullOrEmpty(this.lblLote.Text) && this.TextBoxData.Text.Length > 0 && checkLot != 0) {
                    //Validar lote
                    App.systemCache.MoveLocation.Lotlbl = this.TextBoxData.Text;
                    return string.Empty;
                }
                #endregion

                #region ValidateLocal
                if (string.IsNullOrEmpty(this.lblLocal.Text) && this.TextBoxData.Text.Length > 0) {
                    var result = readsValidationLogic.ValidateLocalRead(this.TextBoxData.Text.Trim());

                    if (!string.IsNullOrEmpty(result.Error)) {
                        return result.Error;
                    }

                    App.systemCache.MoveLocation.Warehouse = result.Warehouse;
                    App.systemCache.MoveLocation.Local = result.Local;
                }
                #endregion

                return string.Empty;
            } catch (Exception ex) {
                logLogic.Insert(ex.Message, LogType.ERRO, MethodBase.GetCurrentMethod().DeclaringType.Name, App.systemCache.UserData.UserName);
                return "Error:" + ex.Message;
            }
        }

        private void TxtCaixaCompleta_TextChanged (object sender, TextChangedEventArgs e) {
            try {
                if (this.txtCaixaCompleta.Text.Trim().Length > 0) {
                    int intValue;
                    if (!int.TryParse(this.txtCaixaCompleta.Text, out intValue)) {
                        DisplayAlert("Localização", "Valor Caixa Completa inválido", "OK");
                        this.txtCaixaCompleta.Text = string.Empty;
                        return;
                    }
                }

                var result = UpdateValues();

                if (!string.IsNullOrEmpty(result)) {
                    DisplayAlert("Localização", "Erro ao calcular quantidades. Erro:" + result, "OK");
                    this.txtCaixaCompleta.Text = string.Empty;
                    return;
                }
            } catch (Exception ex) {
                DisplayAlert("Localização", ex.Message, "OK");
                logLogic.Insert("Erro TxtCaixaCompleta_TextChanged:" + ex.Message, LogType.ERRO, MethodBase.GetCurrentMethod().DeclaringType.Name, App.systemCache.UserData.UserName);
            }
        }

        private void TxtUnAdicionais_TextChanged (object sender, TextChangedEventArgs e) {
            try {
                if (this.txtUnAdicionais.Text.Trim().Length > 0) {
                    int intValue;
                    if (!int.TryParse(this.txtUnAdicionais.Text, out intValue)) {
                        DisplayAlert("Localização", "Valor Un.Adicionais inválido", "OK");
                        this.txtUnAdicionais.Text = string.Empty;
                        return;
                    }
                }

                var result = UpdateValues();

                if (!string.IsNullOrEmpty(result)) {
                    DisplayAlert("Localização", "Erro ao calcular quantidades. Erro:" + result, "OK");
                    this.txtUnAdicionais.Text = string.Empty;
                    return;
                }
            } catch (Exception ex) {
                DisplayAlert("Localização", ex.Message, "OK");
                logLogic.Insert("Erro TxtUnAdicionais_TextChanged:" + ex.Message, LogType.ERRO, MethodBase.GetCurrentMethod().DeclaringType.Name, App.systemCache.UserData.UserName);
            }
        }

        private void LocationsDropDown_SelectedIndexChanged (object sender, EventArgs e) {
            try {
                if (App.systemCache.GlobalConfigsCache.MoveStockLocations.Contains(locationsDropDown.SelectedItem)) {
                    App.systemCache.MoveLocation.Warehouse = App.systemCache.DeviceData.Warehouse;
                    App.systemCache.MoveLocation.Local = locationsDropDown.SelectedItem.ToString();

                    var result = FillStockQuantity();

                    if (!string.IsNullOrEmpty(result)) {
                        DisplayAlert("Localização", result, "OK");
                        return;
                    }

                    this.TextBoxData.Placeholder = string.Empty;
                    this.ButtonOk.IsEnabled = false;
                    this.TextBoxData.Text = string.Empty;
                    this.TextBoxData.IsEnabled = false;
                    this.txtCaixaCompleta.IsEnabled = true;
                    this.ButtonUnidades.IsEnabled = true;
                    this.txtUnAdicionais.IsEnabled = true;
                    this.lblLocal.Text = string.Empty;
                } else {
                    if (!string.IsNullOrEmpty(this.lblProduto.Text)) {
                        this.TextBoxData.Placeholder = "Introduza o local";
                        this.ButtonOk.IsEnabled = false;
                        this.TextBoxData.Text = string.Empty;
                        this.TextBoxData.IsEnabled = true;
                        this.TextBoxData.Focus();
                        this.txtCaixaCompleta.IsEnabled = false;
                        this.ButtonUnidades.IsEnabled = false;
                        this.txtUnAdicionais.IsEnabled = false;
                        this.lblLocal.Text = string.Empty;
                    }
                }
            } catch (Exception ex) {
                DisplayAlert("Localização", ex.Message, "OK");
                logLogic.Insert("Erro LocationsDropDown_SelectedIndexChanged:" + ex.Message, LogType.ERRO, MethodBase.GetCurrentMethod().DeclaringType.Name, App.systemCache.UserData.UserName);
            }
        }
    }
}