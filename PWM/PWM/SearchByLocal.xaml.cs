﻿using Acr.UserDialogs;
using PWM.Logic;
using PWM.Models.Log;
using PWM.Models.Search;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Reflection;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PWM {
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SearchByLocal : ContentPage {
        #region properties
        public int TextBoxLength { get; set; }
        public int rowSize = 50;
        public int columnSize = 100;
        public string Menu { get; set; }
        #endregion

        #region dependencies
        M3Logic m3Logic = new M3Logic();
        LogLogic logLogic = new LogLogic();
        #endregion

        public SearchByLocal (string menu) {
            this.Menu = menu;
            InitializeComponent();
            this.ListViewExpedition.IsVisible = false;
        }

        private void TextBoxFilter_Completed (object sender, System.EventArgs e) {
            FillData();
        }

        private void TextBoxFilter_TextChanged (object sender, TextChangedEventArgs e) {
            if (this.TextBoxFilter.Text.Trim().Length > 0 &&
            this.TextBoxFilter.Text.Trim().Length - this.TextBoxLength != 1 &&
            this.TextBoxFilter.Text.Trim().Length > this.TextBoxLength) {
                FillData();
                this.TextBoxFilter.Focus();
            } else {
                this.TextBoxLength = this.TextBoxFilter.Text.Trim().Length;

                if (this.TextBoxLength > 0) {
                    this.ButtonOk.IsEnabled = true;
                } else {
                    this.ButtonOk.IsEnabled = false;
                }
            }
        }

        private void TextBoxFilter_Unfocused (object sender, FocusEventArgs e) {
            this.TextBoxFilter.Focus();
        }

        private void ButtonOk_Clicked (object sender, System.EventArgs e) {
            FillData();
        }

        private async void FillData () {
            try {
                using (UserDialogs.Instance.Loading("Loading...", null, null, true)) {
                    var response = await Task.Run(() => GetInfo());

                    if (!string.IsNullOrEmpty(response.Item1)) {
                        await DisplayAlert("SearchByLocal", response.Item1, "OK");
                        this.TextBoxFilter.Text = string.Empty;
                        this.labelHeader.Text = string.Empty;
                        return;
                    } else {
                        this.labelHeader.Text = "Local:" + response.Item2;
                        this.TextBoxFilter.Text = string.Empty;

                        var data = new ObservableCollection<Models.Search.SearchByLocalModel>();
                        response.Item3.ForEach(x => data.Add(x));
                        AdjustView(data);
                    }
                }
            } catch (Exception ex) {
                await DisplayAlert("Pesquisar por local", ex.Message, "OK");
                logLogic.Insert("Erro FillData:" + ex.Message, LogType.ERRO, MethodBase.GetCurrentMethod().DeclaringType.Name, App.systemCache.UserData.UserName);
            }
        }

        //Erro, Local, Dados
        private Tuple<string, string, List<SearchByLocalModel>> GetInfo () {
            try {
                var localSplit = this.TextBoxFilter.Text.Trim().Split('|');
                var local = string.Empty;

                if (localSplit.Length == 2) {
                    local = localSplit[1];
                } else {
                    return new Tuple<string, string, List<SearchByLocalModel>>("Local Inválido", string.Empty, null);
                }

                var m3Data = m3Logic.SearchByLocal(App.systemCache.DeviceData.RealEnvironment, App.systemCache.DeviceData.Company, App.systemCache.DeviceData.Facility, local);

                if (!m3Data.Any()) {
                    return new Tuple<string, string, List<SearchByLocalModel>>("Não existe informação para este local", string.Empty, null);
                }

                return new Tuple<string, string, List<SearchByLocalModel>>(string.Empty, local, m3Data);
            } catch (Exception ex) {
                logLogic.Insert(ex.Message, LogType.ERRO, MethodBase.GetCurrentMethod().DeclaringType.Name, App.systemCache.UserData.UserName);
                return new Tuple<string, string, List<SearchByLocalModel>>("Erro:" + ex.Message, string.Empty, null);
            }
        }

        private void AdjustView (ObservableCollection<Models.Search.SearchByLocalModel> data) {
            this.ListViewExpedition.ItemsSource = data;
            var columnsCount = 5;

            this.ListViewExpedition.HeightRequest = rowSize;
            this.ListViewExpedition.WidthRequest = (columnsCount * columnSize);
            this.ListViewExpedition.IsVisible = true;
        }

        private void TapGestureBackButton_Tapped (object sender, System.EventArgs e) {
            var page = Activator.CreateInstance(Type.GetType(this.Menu)) as Page;
            Application.Current.MainPage = page;
        }

        private void ContentPage_Appearing (object sender, System.EventArgs e) {
            this.TextBoxFilter.Focus();
        }
    }
}