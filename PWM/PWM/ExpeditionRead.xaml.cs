﻿using Acr.UserDialogs;
using PWM.Logic;
using PWM.Models;
using PWM.Models.Expedition;
using PWM.Models.Log;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PWM {
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ExpeditionRead : ContentPage {
        #region dependencies
        ExpeditionBlockedLogic expeditionBlockedLogic = new ExpeditionBlockedLogic();
        M3Logic m3Logic = new M3Logic();
        LogLogic logLogic = new LogLogic();
        #endregion

        #region properties
        public int TextBoxLength { get; set; }
        public ObservableCollection<string> expeditionOrderReads { get; set; }
        #endregion

        public ExpeditionRead () {
            this.expeditionOrderReads = new ObservableCollection<string>();
            this.TextBoxLength = 0;
            InitializeComponent();
            this.ButtonConfirm.IsEnabled = false;
            this.listView.ItemsSource = expeditionOrderReads;
        }

        private void ButtonOk_Clicked (object sender, EventArgs e) {
            ValidarEncomenda();
        }

        private void TextBoxEncomenda_TextChanged (object sender, TextChangedEventArgs e) {
            if (this.TextBoxEncomenda.Text.Trim().Length > 0 &&
                this.TextBoxEncomenda.Text.Trim().Length - this.TextBoxLength != 1 &&
                this.TextBoxEncomenda.Text.Trim().Length > this.TextBoxLength) {
                ValidarEncomenda();
                this.TextBoxEncomenda.Focus();
            } else {
                this.TextBoxLength = this.TextBoxEncomenda.Text.Trim().Length;

                if (this.TextBoxLength > 0) {
                    this.ButtonOk.IsEnabled = true;
                } else {
                    this.ButtonOk.IsEnabled = false;
                }
            }
        }

        private async void ButtonConfirm_Clicked (object sender, EventArgs e) {
            try {
                this.ButtonConfirm.IsEnabled = false;
                if (!App.systemCache.ExpeditionData.ExpeditionOrdersPicked.Any()) {
                    await DisplayAlert("Expedição", "Adicione uma encomenda", "OK");
                    return;
                }

                bool answer = await DisplayAlert("Expedição", "Selecionou as encomendas todas que pretendia?", "Sim", "Não");
                if (answer) {
                    var encomendasBloqueadas = expeditionBlockedLogic.Get(App.systemCache.ExpeditionData.ExpeditionOrdersPicked.ToList().Select(x => x.All).ToList());
                    var blockedList = new List<string>();
                    var alreadyInsertedList = new List<string>();

                    if (encomendasBloqueadas.Rows.Count > 0) {
                        foreach (var encomenda in App.systemCache.ExpeditionData.ExpeditionOrdersPicked) {
                            var isBlocked = encomendasBloqueadas.Rows.OfType<DataRow>().Where(x => x["ExpeditionNumber"].ToString() == encomenda.All).Count();

                            if (isBlocked > 0) {
                                var exist = encomendasBloqueadas.Rows.OfType<DataRow>().
                                    Where(x => x["ExpeditionNumber"].ToString() == encomenda.All)
                                    .ToList();

                                if (exist.Count > 0) {
                                    var checkEncomenda = exist.Where(x =>
                                    long.Parse(x["UserID"].ToString()) != App.systemCache.UserData.UserID ||
                                    long.Parse(x["DeviceID"].ToString()) != App.systemCache.DeviceData.DeviceID);

                                    if (checkEncomenda.Count() > 0) {
                                        blockedList.Add(encomenda.All);
                                    } else {
                                        alreadyInsertedList.Add(encomenda.All);
                                    }
                                }
                            }
                        }
                    }

                    if (blockedList.Count > 0) {
                        await DisplayAlert("Expedição", "Encomenda(s) já bloqueada(s) por outro user", "OK");
                        this.ButtonConfirm.IsEnabled = true;
                        return;
                    }

                    try {
                        var insertOrders = App.systemCache.ExpeditionData.ExpeditionOrdersPicked
                            .Where(x => !blockedList.Contains(x.All) && !alreadyInsertedList.Contains(x.All))
                            .Select(x => x.All)
                            .ToList();

                        if (insertOrders.Any()) {
                            expeditionBlockedLogic.Insert(App.systemCache.UserData.UserID, insertOrders, App.systemCache.DeviceData.DeviceID);
                        }
                    } catch (Exception) {
                        await DisplayAlert("Expedição", "Ocorreu um erro ao bloquear as encomendas", "OK");
                        this.ButtonConfirm.IsEnabled = true;
                        return;
                    }

                    var expeditionInfo = m3Logic.GetPackedExpeditions(App.systemCache.DeviceData.RealEnvironment, App.systemCache.DeviceData.Company, App.systemCache.DeviceData.Warehouse, App.systemCache.ExpeditionData.ExpeditionInfo.Select(x => x.DeliveryNumber).Distinct().ToList());

                    foreach (var exp in expeditionInfo) {
                        if (App.systemCache.ExpeditionData.DeliveryBoxDic.ContainsKey(exp.DeliveryNumber)) {
                            App.systemCache.ExpeditionData.DeliveryBoxDic[exp.DeliveryNumber].Add(exp.Box);
                        } else {
                            App.systemCache.ExpeditionData.DeliveryBoxDic.Add(exp.DeliveryNumber, new List<string> { exp.Box });
                        }
                    }

                    Application.Current.MainPage = new ExpeditionMenu();
                } else {
                    this.ButtonConfirm.IsEnabled = true;
                }
            } catch (Exception ex) {
                logLogic.Insert(ex.Message, LogType.ERRO, MethodBase.GetCurrentMethod().DeclaringType.Name, App.systemCache.UserData.UserName);
            }
        }

        private void TextBoxEncomenda_Unfocused (object sender, FocusEventArgs e) {
            this.TextBoxEncomenda.Focus();
        }

        private void ContentPage_Appearing (object sender, EventArgs e) {
            this.TextBoxEncomenda.Focus();
        }

        #region private methods
        private void AdjustListViewSize () {
            var heightRow = this.listView.RowHeight;
            var rowsCount = App.systemCache.ExpeditionData.ExpeditionOrdersPicked.Count;

            this.listView.HeightRequest = (rowsCount * heightRow);
        }

        private async void ValidarEncomenda () {
            using (UserDialogs.Instance.Loading("Loading...", null, null, true)) {
                try {
                    this.ButtonOk.IsEnabled = false;
                    this.ButtonConfirm.IsEnabled = false;

                    var response = await Task.Run(() => Validate());

                    if (!string.IsNullOrEmpty(response.Item1)) {
                        this.ButtonOk.IsEnabled = true;
                        this.ButtonConfirm.IsEnabled = true;
                        this.TextBoxEncomenda.Text = string.Empty;
                        await DisplayAlert("ExpediçãoLeitura", response.Item1, "OK");
                        return;
                    } else {
                        App.systemCache.ExpeditionData.ExpeditionOrdersPicked.Add(response.Item2);
                        this.expeditionOrderReads.Add(this.TextBoxEncomenda.Text.Trim());
                        App.systemCache.ExpeditionData.ExpeditionInfo.AddRange(response.Item3);

                        this.TextBoxEncomenda.Text = string.Empty;
                        AdjustListViewSize();
                    }

                    this.ButtonConfirm.IsEnabled = true;
                    this.ButtonOk.IsEnabled = true;
                } catch (Exception ex) {
                    await DisplayAlert("Expedicao", ex.Message, "OK");
                    logLogic.Insert("Erro ValidarEncomenda:" + ex.Message, LogType.ERRO, MethodBase.GetCurrentMethod().DeclaringType.Name, App.systemCache.UserData.UserName);
                }
            }
        }

        private Tuple<string, ExpeditionReadModel, List<ExpeditionModel>> Validate () {
            var readPoco = new ExpeditionReadModel();
            var getExpedition = new List<ExpeditionModel>();

            try {
                if (string.IsNullOrEmpty(this.TextBoxEncomenda.Text)) {
                    return new Tuple<string, ExpeditionReadModel, List<ExpeditionModel>>("Adicione uma encomenda", null, null);
                }

                if (this.TextBoxEncomenda.Text.Trim().Length > 0) {
                    var splitData = this.TextBoxEncomenda.Text.Trim().Split('|');

                    if (splitData.Length != 3) {
                        return new Tuple<string, ExpeditionReadModel, List<ExpeditionModel>>("Encomenda Inválida", null, null);
                    }

                    long encomenda;
                    long entrega;
                    int sufixo;

                    if (!long.TryParse(splitData[0], out encomenda) ||
                        !long.TryParse(splitData[1], out entrega) ||
                        !int.TryParse(splitData[2], out sufixo)) {
                        return new Tuple<string, ExpeditionReadModel, List<ExpeditionModel>>("Encomenda Inválida", null, null);
                    }

                    if (App.systemCache.ExpeditionData.ExpeditionOrdersPicked.Where(x => x.Encomenda == encomenda && x.Entrega == entrega && x.Sufixo == sufixo).Count() > 0) {
                        return new Tuple<string, ExpeditionReadModel, List<ExpeditionModel>>("Encomenda já adicionada", null, null);
                    }

                    readPoco = new ExpeditionReadModel() {
                        Sufixo = sufixo,
                        Encomenda = encomenda,
                        Entrega = entrega,
                        All = this.TextBoxEncomenda.Text.Trim()
                    };

                    getExpedition = m3Logic.GetExpeditions(
                        App.systemCache.DeviceData.RealEnvironment,
                        App.systemCache.DeviceData.Company,
                        App.systemCache.DeviceData.Division,
                        App.systemCache.DeviceData.Facility,
                        App.systemCache.DeviceData.Warehouse,
                        readPoco,
                        App.systemCache.GlobalConfigsCache.IgnoreAlias);

                    if (!getExpedition.Any()) {
                        return new Tuple<string, ExpeditionReadModel, List<ExpeditionModel>>("Encomenda|Entrega|Sufixo inexistente ou sem entregas pendentes no armazém", null, null);
                    }
                }

                return new Tuple<string, ExpeditionReadModel, List<ExpeditionModel>>(string.Empty, readPoco, getExpedition);
            } catch (Exception ex) {
                logLogic.Insert(ex.Message, LogType.ERRO, MethodBase.GetCurrentMethod().DeclaringType.Name, App.systemCache.UserData.UserName);
                return new Tuple<string, ExpeditionReadModel, List<ExpeditionModel>>("Erro:" + ex.Message, null, null);
            }
        }
        #endregion

        private async void TapGestureBackButton_Tapped (object sender, EventArgs e) {
            if (App.systemCache.ExpeditionData.ExpeditionOrdersPicked.Any()) {
                bool answer = await DisplayAlert("Expedição - Leituras", "Tem leituras de expedição. Se voltar atrás irá limpar todas as leituras. Quer voltar?", "Sim", "Não");
                if (answer) {
                    App.systemCache.ExpeditionClearAllData();
                    Application.Current.MainPage = new MenuMenu();
                } else {
                    return;
                }
            }

            Application.Current.MainPage = new MenuMenu();
        }

        private async void ListView_ItemTapped (object sender, ItemTappedEventArgs e) {
            try {
                var read = ((ListView)sender).SelectedItem.ToString();
                bool answer = await DisplayAlert("Expedição", "Tem certeza que quer eliminar a leitura " + read + "?", "Sim", "Não");
                if (answer) {
                    this.expeditionOrderReads.Remove(read);
                    this.listView.ItemsSource = expeditionOrderReads;

                    var item = App.systemCache.ExpeditionData.ExpeditionOrdersPicked.Where(x => x.All == read).FirstOrDefault();
                    App.systemCache.ExpeditionData.ExpeditionOrdersPicked.Remove(item);
                    AdjustListViewSize();

                    if (!this.expeditionOrderReads.Any()) {
                        this.ButtonConfirm.IsEnabled = false;
                    }
                } else {
                    ((ListView)sender).SelectedItem = null;
                }
            } catch (Exception) {
                await DisplayAlert("Expedição", "Ocorreu um erro ao tentar remover a ordem", "OK");
            }
        }
    }
}