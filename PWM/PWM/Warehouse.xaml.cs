﻿using Acr.UserDialogs;
using PWM.Logic;
using PWM.Models.Device;
using PWM.Models.Log;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PWM {
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Warehouse : ContentPage {
        #region dependencies
        M3Logic m3Logic = new M3Logic();
        DeviceConfigLogic deviceConfigLogic = new DeviceConfigLogic();
        LogLogic logLogic = new LogLogic();
        #endregion

        #region properties
        DataTable warehouses = new DataTable();
        #endregion

        public Warehouse () {
            InitializeComponent();
            FillDataAndAdjustListViewSize();
        }

        private void FillDataAndAdjustListViewSize () {
            using (UserDialogs.Instance.Loading("Loading...", null, null, true)) {
                try {
                    warehouses = m3Logic.GetWarehouses(App.systemCache.DeviceData.RealEnvironment, App.systemCache.DeviceData.Facility);

                    var data = new ObservableCollection<string>();
                    warehouses.Rows.OfType<DataRow>().ToList().ForEach(x => data.Add(x["Warehouse"].ToString()));
                    this.listView.ItemsSource = data;

                    var heightRow = this.listView.RowHeight;
                    var rowsCount = warehouses.Rows.Count;

                    this.listView.HeightRequest = (rowsCount * heightRow);
                } catch (Exception ex) {
                    DisplayAlert("Armazem", ex.Message, "OK");
                    logLogic.Insert("Erro FillDataAndAdjustListViewSize:" + ex.Message, LogType.ERRO, MethodBase.GetCurrentMethod().DeclaringType.Name, App.systemCache.UserData.UserName);
                }
            }
        }

        private async void GravarButton_Clicked (object sender, EventArgs e) {
            using (UserDialogs.Instance.Loading("Loading...", null, null, true)) {
                var response = await Task.Run(() => ChangeWarehouse());

                if (!string.IsNullOrEmpty(response)) {
                    await DisplayAlert("Warehouse", response, "OK");
                    return;
                }

                await DisplayAlert("Warehouse", "Alteração feita com sucesso.", "OK");
                Application.Current.MainPage = new MenuMenu();
            }
        }

        private string ChangeWarehouse () {
            try {
                if (this.listView.SelectedItem == null) {
                    return "Selecione um armazém";
                }

                try {
                    var oldWarehouse = App.systemCache.DeviceData.Warehouse;

                    var listToUpdate = new List<UpdateConfigModel>();
                    listToUpdate.Add(new UpdateConfigModel() {
                        ConfigID = App.systemCache.DeviceData.ConfigsID[ConfigEnum.WarehouseConfig],
                        ConfigValue = this.listView.SelectedItem.ToString().Trim(),
                        DeviceID = App.systemCache.DeviceData.DeviceID
                    });

                    App.systemCache.DeviceData.Warehouse = this.listView.SelectedItem.ToString().Trim();
                } catch (Exception ex) {
                    logLogic.Insert(ex.Message, LogType.ERRO, MethodBase.GetCurrentMethod().DeclaringType.Name, App.systemCache.UserData.UserName);
                    return "Ocorreu um erro ao tentar mudar de armazém. Contacte o admnistrador.";
                }

                return string.Empty;
            } catch (Exception ex) {
                DisplayAlert("Armazem", ex.Message, "OK");
                logLogic.Insert("Erro ChangeWarehouse:" + ex.Message, LogType.ERRO, MethodBase.GetCurrentMethod().DeclaringType.Name, App.systemCache.UserData.UserName);
                return ex.Message;
            }
        }
    }
}