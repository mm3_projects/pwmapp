package md578dd6ebe565959e9d12124448b78c507;


public class AutoCompleteTextViewRenderer
	extends md51558244f76c53b6aeda52c8a337f2c37.ViewRenderer_2
	implements
		mono.android.IGCUserPeer
{
/** @hide */
	public static final String __md_methods;
	static {
		__md_methods = 
			"";
		mono.android.Runtime.register ("Xamarin.RisePlugin.AutoCompleteTextView.Droid.AutoCompleteTextViewRenderer, Xamarin.RisePlugin.AutoCompleteTextView.Droid", AutoCompleteTextViewRenderer.class, __md_methods);
	}


	public AutoCompleteTextViewRenderer (android.content.Context p0)
	{
		super (p0);
		if (getClass () == AutoCompleteTextViewRenderer.class)
			mono.android.TypeManager.Activate ("Xamarin.RisePlugin.AutoCompleteTextView.Droid.AutoCompleteTextViewRenderer, Xamarin.RisePlugin.AutoCompleteTextView.Droid", "Android.Content.Context, Mono.Android", this, new java.lang.Object[] { p0 });
	}


	public AutoCompleteTextViewRenderer (android.content.Context p0, android.util.AttributeSet p1)
	{
		super (p0, p1);
		if (getClass () == AutoCompleteTextViewRenderer.class)
			mono.android.TypeManager.Activate ("Xamarin.RisePlugin.AutoCompleteTextView.Droid.AutoCompleteTextViewRenderer, Xamarin.RisePlugin.AutoCompleteTextView.Droid", "Android.Content.Context, Mono.Android:Android.Util.IAttributeSet, Mono.Android", this, new java.lang.Object[] { p0, p1 });
	}


	public AutoCompleteTextViewRenderer (android.content.Context p0, android.util.AttributeSet p1, int p2)
	{
		super (p0, p1, p2);
		if (getClass () == AutoCompleteTextViewRenderer.class)
			mono.android.TypeManager.Activate ("Xamarin.RisePlugin.AutoCompleteTextView.Droid.AutoCompleteTextViewRenderer, Xamarin.RisePlugin.AutoCompleteTextView.Droid", "Android.Content.Context, Mono.Android:Android.Util.IAttributeSet, Mono.Android:System.Int32, mscorlib", this, new java.lang.Object[] { p0, p1, p2 });
	}

	private java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
