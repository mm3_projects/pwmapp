﻿using Android.Bluetooth;
using Java.Util;
using PWM.Core;
using PWM.Droid;
using System;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

[assembly: Xamarin.Forms.Dependency(typeof(BluetoothPrinter))]
namespace PWM.Droid {
    public class BluetoothPrinter : IBluetoothPrinter
    {
        public string Print(string deviceName, string text) {
            try {
                BluetoothAdapter bluetoothAdapter = BluetoothAdapter.DefaultAdapter;

                var alldevices = bluetoothAdapter.BondedDevices.ToList();
                BluetoothDevice device = (from bd in bluetoothAdapter.BondedDevices
                                          where bd?.Name == deviceName
                                          select bd).FirstOrDefault();

                if (device == null)
                {
                    return "Impressora não está ligada/emparelhada";
                }

                BluetoothSocket bluetoothSocket = device.
                                  CreateRfcommSocketToServiceRecord(
                                  UUID.FromString("00001101-0000-1000-8000-00805f9b34fb"));

                bluetoothSocket.Connect();
                Thread.Sleep(new TimeSpan(0, 0, 2));

                byte[] buffer = Encoding.UTF8.GetBytes(text);
                bluetoothSocket.OutputStream.Write(buffer, 0, buffer.Length);
                bluetoothSocket.Close();
                
                return string.Empty;
            } catch(Exception ex) {
                return ex.Message;
            }
        }
    }
}