﻿using Android.Content.PM;
using PWM.Core;
using PWM.Droid;
using Xamarin.Forms;

[assembly: Dependency(typeof(AppVersion))]
namespace PWM.Droid {
    public class AppVersion : IAppVersion {
        PackageInfo appInfo;
        public AppVersion() {
            var context = Android.App.Application.Context;
            appInfo = context.PackageManager.GetPackageInfo(context.PackageName, 0);
        }

        public string GetVersionNumber() {
            return appInfo.VersionName;
        }
    }
}